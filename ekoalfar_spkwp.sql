-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 06 Agu 2018 pada 04.02
-- Versi server: 5.7.19
-- Versi PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekoalfar_spkwp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_bobot_kriteria`
--

CREATE TABLE `tbl_bobot_kriteria` (
  `id_kriteria` varchar(3) NOT NULL,
  `nama_kriteria` varchar(20) NOT NULL,
  `bobot_awal` int(1) DEFAULT NULL,
  `bobot_perbaikan` float(8,6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_bobot_kriteria`
--

INSERT INTO `tbl_bobot_kriteria` (`id_kriteria`, `nama_kriteria`, `bobot_awal`, `bobot_perbaikan`) VALUES
('K01', 'Minat Siswa', 5, 0.227273),
('K02', 'Minat Orang Tua', 3, 0.136364),
('K03', 'Minat Mata Pelajaran', 5, 0.227273),
('K04', 'Rerata Rapor', 4, 0.181818),
('K05', 'Nilai UN/US', 5, 0.227273);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil`
--

CREATE TABLE `tbl_hasil` (
  `id_hasil` int(5) NOT NULL,
  `id_alternatif` int(11) NOT NULL,
  `s_mipa` float(7,5) NOT NULL,
  `s_iis` float(7,5) NOT NULL,
  `v_mipa` float(7,5) NOT NULL,
  `v_iis` float(7,5) NOT NULL,
  `jurusan_prioritas` varchar(4) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_hasil`
--

INSERT INTO `tbl_hasil` (`id_hasil`, `id_alternatif`, `s_mipa`, `s_iis`, `v_mipa`, `v_iis`, `jurusan_prioritas`, `created_at`, `updated_at`) VALUES
(1, 1, 8.19826, 8.08475, 0.50349, 0.49651, 'MIPA', '2018-08-06 02:45:14', '0000-00-00 00:00:00'),
(2, 7, 7.92851, 8.35450, 0.48692, 0.51308, 'IIS', '2018-08-06 02:45:14', '0000-00-00 00:00:00'),
(4, 9, 7.74935, 8.14081, 0.48768, 0.51232, 'IIS', '2018-08-06 02:45:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(8) NOT NULL,
  `judul_menu` varchar(30) NOT NULL,
  `link` varchar(30) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `is_main_menu` int(8) NOT NULL,
  `level` enum('ADMIN','GURU','SISWA') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `judul_menu`, `link`, `icon`, `is_main_menu`, `level`) VALUES
(1, 'data peserta didik', 'data-siswa', 'fa-file-archive-o', 0, 'ADMIN'),
(2, 'data users', 'data-users', 'fa-users', 0, 'ADMIN'),
(3, 'laporan', 'laporan', 'fa-file-pdf-o', 0, 'ADMIN'),
(4, 'data nilai', 'data-nilai', 'fa-book', 0, 'GURU'),
(5, 'input nilai', 'cari', 'fa-pencil-square-o', 0, 'GURU'),
(6, 'bobot kriteria', 'input-bobot', 'fa fa-file-excel-o', 0, 'GURU'),
(7, 'perhitungan', 'perhitungan', 'fa-cogs', 0, 'GURU'),
(8, 'laporan', 'laporan', 'fa-file-pdf-o', 0, 'GURU'),
(9, 'input angket', 'angket', 'fa-pencil-square-o', 0, 'SISWA'),
(10, 'hasil analisa', 'hasil-analisa', 'fa-envelope-square', 0, 'SISWA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_nilai`
--

CREATE TABLE `tbl_nilai` (
  `id_nilai` int(11) NOT NULL,
  `nisn` char(10) NOT NULL,
  `ipasm1` float(4,2) NOT NULL,
  `ipasm2` float(4,2) NOT NULL,
  `ipasm3` float(4,2) NOT NULL,
  `ipasm4` float(4,2) NOT NULL,
  `ipasm5` float(4,2) NOT NULL,
  `ipasm6` float(4,2) NOT NULL,
  `ipssm1` float(4,2) NOT NULL,
  `ipssm2` float(4,2) NOT NULL,
  `ipssm3` float(4,2) NOT NULL,
  `ipssm4` float(4,2) NOT NULL,
  `ipssm5` float(4,2) NOT NULL,
  `ipssm6` float(4,2) NOT NULL,
  `mtksm1` float(4,2) NOT NULL,
  `mtksm2` float(4,2) NOT NULL,
  `mtksm3` float(4,2) NOT NULL,
  `mtksm4` float(4,2) NOT NULL,
  `mtksm5` float(4,2) NOT NULL,
  `mtksm6` float(4,2) NOT NULL,
  `rata_ipa` float(5,3) NOT NULL,
  `rata_ips` float(5,3) NOT NULL,
  `un_ipa` float(4,2) NOT NULL,
  `us_ips` float(4,2) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_nilai`
--

INSERT INTO `tbl_nilai` (`id_nilai`, `nisn`, `ipasm1`, `ipasm2`, `ipasm3`, `ipasm4`, `ipasm5`, `ipasm6`, `ipssm1`, `ipssm2`, `ipssm3`, `ipssm4`, `ipssm5`, `ipssm6`, `mtksm1`, `mtksm2`, `mtksm3`, `mtksm4`, `mtksm5`, `mtksm6`, `rata_ipa`, `rata_ips`, `un_ipa`, `us_ips`, `created_at`, `updated_at`) VALUES
(1, '1234567890', 85.00, 90.00, 84.00, 90.00, 80.00, 80.00, 90.00, 85.00, 90.00, 83.00, 90.00, 86.00, 75.00, 78.00, 72.00, 75.00, 72.00, 74.00, 79.583, 80.833, 80.67, 78.00, '2018-07-20 01:42:27', '2018-07-19 17:00:00'),
(3, '2345678901', 85.00, 90.00, 84.00, 90.00, 80.00, 80.00, 90.00, 85.00, 90.00, 83.00, 90.00, 86.00, 75.00, 78.00, 72.00, 75.00, 72.00, 74.00, 79.583, 80.833, 80.67, 78.00, '2018-08-05 00:42:38', '2018-08-05 14:48:11'),
(5, '0134567890', 78.53, 80.10, 79.80, 77.50, 78.90, 80.35, 79.77, 80.50, 81.45, 81.10, 84.13, 85.00, 75.66, 76.00, 78.70, 77.00, 75.00, 76.10, 77.803, 79.201, 60.67, 55.00, '2018-08-06 02:37:53', '2018-08-06 02:42:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_nilai_alternatif`
--

CREATE TABLE `tbl_nilai_alternatif` (
  `id_alternatif` int(8) NOT NULL,
  `nisn` char(10) NOT NULL,
  `k1_mipa` int(1) NOT NULL,
  `k2_mipa` int(1) NOT NULL,
  `k3_mipa` int(1) NOT NULL,
  `k4_mipa` float(7,5) NOT NULL,
  `k5_mipa` float(7,5) NOT NULL,
  `k1_iis` int(1) NOT NULL,
  `k2_iis` int(1) NOT NULL,
  `k3_iis` int(1) NOT NULL,
  `k4_iis` float(7,5) NOT NULL,
  `k5_iis` float(7,5) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_nilai_alternatif`
--

INSERT INTO `tbl_nilai_alternatif` (`id_alternatif`, `nisn`, `k1_mipa`, `k2_mipa`, `k3_mipa`, `k4_mipa`, `k5_mipa`, `k1_iis`, `k2_iis`, `k3_iis`, `k4_iis`, `k5_iis`, `created_at`, `updated_at`) VALUES
(1, '1234567890', 1, 2, 2, 79.58333, 80.67000, 2, 1, 1, 80.83333, 78.00000, '2018-07-20 01:48:00', '2018-07-19 17:00:00'),
(7, '2345678901', 1, 1, 1, 79.58333, 80.67000, 2, 2, 2, 80.83333, 78.00000, '2018-08-05 14:48:11', '0000-00-00 00:00:00'),
(9, '0134567890', 1, 1, 1, 77.80333, 60.67000, 2, 2, 2, 79.20083, 55.00000, '2018-08-06 02:42:46', '2018-08-06 02:43:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_siswa`
--

CREATE TABLE `tbl_siswa` (
  `nisn` char(10) NOT NULL,
  `id_users` varchar(5) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL DEFAULT '0',
  `tempat_lahir` varchar(50) NOT NULL DEFAULT '0',
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `asal_sekolah` varchar(30) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '0',
  `tahun_ajaran` varchar(9) NOT NULL,
  `minat_mapel` varchar(70) NOT NULL,
  `minat_jurusan` char(4) NOT NULL,
  `minat_ortu` char(4) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabel peserta didik';

--
-- Dumping data untuk tabel `tbl_siswa`
--

INSERT INTO `tbl_siswa` (`nisn`, `id_users`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `alamat`, `asal_sekolah`, `password`, `tahun_ajaran`, `minat_mapel`, `minat_jurusan`, `minat_ortu`, `created_at`, `updated_at`) VALUES
('0134567890', 'ADM01', 'Superman', 'Brebes', '2002-08-16', 'Jalan Wacung No. 80', 'SMP NEGERI 10 BREBES', '$2y$10$/zVbV6ybDnNeVo6aB9bqduekA5Ek05aPjA2VwVTdvP.Uigf6sbKfS', '2018/2019', 'Geografi, Sejarah', 'iis', 'iis', '2018-08-06 02:37:53', '2018-08-06 03:24:28'),
('1234567890', 'ADM01', 'Dika Permana', 'Jakarta', '2002-12-10', 'Jalan Tanjung Pasir No. 130 Jakarta Utara', 'SMP Wijayakusuma', '$2y$10$PPl/U/tBEWPZWX3ad.atu.4bCBVvNfm/iVIjRMjKKJ560OeBhx4Fm', '2018/2019', 'Geografi, Sejarah', 'iis', 'mipa', '2018-07-30 08:33:00', '2018-08-04 07:21:39'),
('2345678901', 'ADM01', 'Vio Anang Saputra', 'Jakarta', '2003-08-05', 'Jalan Xxx', 'SMP WIJAYAKUSUMA', '$2y$10$FIQ5kf.DLL0lmQ5Tl1EuO.JRfWEGvLFUmO5Vagw6sS7Y1/axTuhqq', '2018/2019', 'Ekonomi, Geografi, Sejarah, Matematika', 'iis', 'iis', '2018-08-05 00:42:38', '2018-08-05 14:12:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id_users` varchar(5) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `level` enum('ADMIN','GURU') NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`id_users`, `username`, `password`, `nama_lengkap`, `level`, `created_at`, `updated_at`) VALUES
('ADM01', 'admin', '$2y$10$5ohVijW0mtVDFgz08G6oB.QhW8OeB.soTbsVHkiWKQeZEWDEovsDq', 'Subekti, S.Pd', 'ADMIN', '2018-07-14 02:00:00', '2018-08-06 10:17:10'),
('GUR01', 'yurike', '$2y$10$/EmponEB58U7.1/2k20bfuG8jkVgdAU8j1i4w7fzy5autr.zNyl62', 'Yurike Budiargo, S.Psi', 'GURU', '2018-07-19 17:14:41', '2018-08-06 10:21:21');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_bobot_kriteria`
--
ALTER TABLE `tbl_bobot_kriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indeks untuk tabel `tbl_hasil`
--
ALTER TABLE `tbl_hasil`
  ADD PRIMARY KEY (`id_hasil`),
  ADD UNIQUE KEY `id_alternatif` (`id_alternatif`) USING BTREE;

--
-- Indeks untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `tbl_nilai`
--
ALTER TABLE `tbl_nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `nisn` (`nisn`) USING BTREE;

--
-- Indeks untuk tabel `tbl_nilai_alternatif`
--
ALTER TABLE `tbl_nilai_alternatif`
  ADD PRIMARY KEY (`id_alternatif`),
  ADD KEY `nisn` (`nisn`) USING BTREE,
  ADD KEY `nisn_2` (`nisn`);

--
-- Indeks untuk tabel `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  ADD PRIMARY KEY (`nisn`),
  ADD KEY `id_users` (`id_users`);

--
-- Indeks untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_hasil`
--
ALTER TABLE `tbl_hasil`
  MODIFY `id_hasil` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_nilai`
--
ALTER TABLE `tbl_nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_nilai_alternatif`
--
ALTER TABLE `tbl_nilai_alternatif`
  MODIFY `id_alternatif` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_hasil`
--
ALTER TABLE `tbl_hasil`
  ADD CONSTRAINT `tbl_hasil_ibfk_1` FOREIGN KEY (`id_alternatif`) REFERENCES `tbl_nilai_alternatif` (`id_alternatif`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_nilai`
--
ALTER TABLE `tbl_nilai`
  ADD CONSTRAINT `tbl_nilai_ibfk_1` FOREIGN KEY (`nisn`) REFERENCES `tbl_siswa` (`nisn`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_nilai_alternatif`
--
ALTER TABLE `tbl_nilai_alternatif`
  ADD CONSTRAINT `tbl_nilai_alternatif_ibfk_1` FOREIGN KEY (`nisn`) REFERENCES `tbl_siswa` (`nisn`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  ADD CONSTRAINT `tbl_siswa_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `tbl_users` (`id_users`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
