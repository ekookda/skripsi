<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model 
{

	public function get_data($table, $where)
	{
		$this->db->where($where);
		$query = $this->db->get($table);

		return $query;
	}

	public function updated_at($table, $where, $data)
	{
		$this->db->where($where);

		return $update = $this->db->update($table, $data);
	}

}

/* End of file M_login.php */
