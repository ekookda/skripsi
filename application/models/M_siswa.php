<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_siswa extends CI_Model 
{
	public $table = 'tbl_siswa';

	public function set_angket($data, $where)
	{
		$this->db->where($where);
		return $this->db->update($this->table, $data);
	}

	public function set_data_update($table, $data, $where)
	{
		$this->db->where($where);
		$query = $this->db->update($table, $data);
		
		return $query;
	}
	
	public function get_where($tables, $where)
	{
		$db = $this->db->get_where($tables, $where);
		return $db;
	}

	public function hasilAnalisa($where)
	{
		$this->db->where($where)
				 ->select('*')
				 ->from('tbl_nilai_alternatif')
				 ->join('tbl_siswa', 'tbl_siswa.nisn = tbl_nilai_alternatif.nisn')
				 ->join('tbl_nilai', 'tbl_nilai.nisn = tbl_nilai_alternatif.nisn')
				 ->join('tbl_hasil', 'tbl_hasil.id_alternatif = tbl_nilai_alternatif.id_alternatif');
		return $this->db->get();
	}

}

/* End of file M_siswa.php */
