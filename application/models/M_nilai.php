<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_nilai extends CI_Model 
{

	public function get_dataNilai()
	{
		return $this->db->select('tbl_siswa.nama_lengkap, tbl_siswa.nisn, tbl_nilai.rata_ipa, tbl_nilai.rata_ips, 
								tbl_nilai.un_ipa, tbl_nilai.us_ips')
						->from('tbl_siswa')
						->join('tbl_nilai', 'tbl_siswa.nisn = tbl_nilai.nisn')
						->get();
	}

	public function get_nilaiAlt()
	{
		return $this->db->select('tbl_siswa.nama_lengkap, tbl_siswa.nisn, tbl_nilai_alternatif.*')
						->from('tbl_siswa')
						->join('tbl_nilai_alternatif', 'tbl_siswa.nisn = tbl_nilai_alternatif.nisn')
						->get();
	}

	public function get_hasilAnalisa()
	{
		return $this->db->select('tbl_siswa.nama_lengkap, tbl_siswa.nisn, tbl_nilai_alternatif.*, tbl_hasil.*')
						->from('tbl_siswa')
						->join('tbl_nilai_alternatif', 'tbl_siswa.nisn = tbl_nilai_alternatif.nisn')
						->join('tbl_hasil', 'tbl_hasil.id_alternatif = tbl_nilai_alternatif.id_alternatif')
						->get();
	}

	public function set_hasilAnalisa($data)
	{
		return $this->db->insert('tbl_hasil', $data);
	}

}

/* End of file M_nilai.php */
