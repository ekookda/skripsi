<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_guru extends CI_Model
{

	public function get_data($table)
	{
		$this->db->order_by('created_at', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function set_data($table, $data)
	{
		$insert = $this->db->insert($table, $data);
		
		return $insert;
	}

	public function get_data_where($table, $where)
	{
		$query = $this->db->get_where($table, $where);
		
		return $query;
	}

	public function set_data_update($table, $data, $where)
	{
		$this->db->where($where);
		$query = $this->db->update($table, $data);

		return $query;
	}
	
	public function set_delete($table, $where)
	{
		$this->db->where($where);
		$query = $this->db->delete($table);

		return $query;
	}
	
	public function get_last_id_users($level)
	{
		$this->db->select_max('id_users');
		$this->db->where(array('level'=>$level));
		$query = $this->db->get('tbl_users');
		
		return $query;
	}

	public function detailSiswa($where)
	{
		$this->db->where($where)
				 ->select('*')
				 ->from('tbl_nilai_alternatif')
				 ->join('tbl_siswa', 'tbl_siswa.nisn = tbl_nilai_alternatif.nisn')
				 ->join('tbl_nilai', 'tbl_nilai.nisn = tbl_nilai_alternatif.nisn')
				 ->join('tbl_hasil', 'tbl_hasil.id_alternatif = tbl_nilai_alternatif.id_alternatif');
		return $this->db->get();
	}

	public function get_search($where)
	{
		return $this->db->where($where)
						->select('*')
						->from('tbl_nilai')
						->join('tbl_siswa', 'tbl_siswa.nisn = tbl_nilai.nisn')
						->get();
	}

}

/* End of file M_guru.php */
