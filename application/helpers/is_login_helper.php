<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Helper untuk mengecek sudah login atau belum
if (!function_exists('isLoggedIn'))
{
	function isLoggedIn($isLogin)
	{
		if (!isset($isLogin) || $isLogin == FALSE)
		{
			$this->session->set_flashdata('notif', 'Anda belum melakukan login!');
			redirect('welcome');
		}
	}
}
