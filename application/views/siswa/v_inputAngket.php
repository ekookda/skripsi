<?php
$this->load->view('template/1_header.php');
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>
<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-sm-12">

		<?php if ($this->session->flashdata('notif') || validation_errors()): ?>

			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?= $this->session->flashdata('notif'); ?>
				<?= validation_errors(); ?>
			</div>

		<?php elseif ($this->session->flashdata('success')): ?>

			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?= $this->session->flashdata('success'); ?>
			</div>

		<?php endif; ?>

		</div>
	</div>

    <!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-edit"></i> <?= $box_title; ?></h3>
		</div>  <!-- /.box-header -->

		<div class="box-body">
			<table border="0" class="table table-condensed table-hover">
				<?php echo form_open('siswa/isi-angket', ['role'=>'form']); ?>
				<tr>
					<th><?php echo form_label('Jurusan yang anda minati', 'minat_jurusan', ['class'=>'label-control']); ?></th>
					<td class="text-center">:</td>
					<td class="text-left"><?php echo form_radio('minat_jurusan', 'mipa', '', ['class'=>'', 'id'=>'minat_mipa']); ?> MIPA</td>
					<td class="text-left"><?php echo form_radio('minat_jurusan', 'iis', '', ['class'=>'', 'id'=>'minat_iis']); ?> IIS</td>
				</tr>
				<tr>
					<th><?php echo form_label('Jurusan yang dipilih orang tua', 'minat_orangtua', ['class'=>'label-control']); ?></th>
					<td class="text-center">:</td>
					<td class="text-left"><?php echo form_radio('minat_orangtua', 'mipa', '', ['class'=>'', 'id'=>'ortu_mipa']); ?> MIPA</td>
					<td class="text-left"><?php echo form_radio('minat_orangtua', 'iis', '', ['class'=>'', 'id'=>'ortu_iis']); ?> IIS</td>
				</tr>
				<tr>
					<th><?php echo form_label('Mata pelajaran yang diminati', 'minat_mapel', ['class'=>'label-control']); ?></th>
					<td class="text-center">:</td>
					<td class="text-left">
						<?php 
						echo form_checkbox('mapel_mipa[]', 'Matematika', '', ['class'=>'']);
						echo form_label('Matematika');

						echo "<br>";

						echo form_checkbox('mapel_mipa[]', 'Biologi', '', ['class'=>'']);
						echo form_label('Biologi');

						echo "<br>";
						
						echo form_checkbox('mapel_mipa[]', 'Kimia', '', ['class'=>'']);
						echo form_label('Kimia');
						
						echo "<br>";
						
						echo form_checkbox('mapel_mipa[]', 'Fisika', '', ['class'=>'']);
						echo form_label('Fisika');
						?>
					</td>
					<td class="text-left">
						<?php 
						echo form_checkbox('mapel_iis[]', 'Ekonomi', '', ['class'=>'']); 
						echo form_label('Ekonomi');

						echo "<br>";
						
						echo form_checkbox('mapel_iis[]', 'Geografi', '', ['class'=>'']); 
						echo form_label('Geografi');
						
						echo "<br>";
						
						echo form_checkbox('mapel_iis[]', 'Sejarah', '', ['class'=>'']); 
						echo form_label('Sejarah');
						
						echo "<br>";
						
						echo form_checkbox('mapel_iis[]', 'Sosiologi', '', ['class'=>'']);
						echo form_label('Sosiologi');
						?>
					</td>
				</tr>
				<tfooter>
				<tr>
					<td colspan="2"></td>
					<td>
						<?php
						echo form_submit('btn_angket', 'Simpan', ['id'=>'btn_angket', 'class'=>'btn btn-primary']);
						$url = $this->input->server('HTTP_REFERER');
						?>
						<a href="<?=$url;?>" class="btn btn-danger">Batal</a>
					</td>
				</tr>
				</tfooter>
				<?php echo form_close(); ?>
			</table> <!-- /.table -->			
		</div> <!-- /.box-body -->
	</div> <!-- /.box -->

</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	$('#example1').dataTable({
		
	});

	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);

});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
