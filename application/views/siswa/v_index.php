<?php
$this->load->view('template/1_header.php');
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>
    <!-- Main content -->
    <section class="content">

    <!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">
			<?php
				$title_header = (!empty($title_header)) ? $title_header : NULL;
				echo $title_header;
			?> 
			</h3>
		</div>
		<div class="box-body">
			<?php
			$body = (!empty($box_body)) ? $box_body : NULL;
			echo "<h4>".$body."</h4>";
			?>
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			
		</div>
		<!-- /.box-footer-->
		</div>
		<!-- /.box -->

	</section>
	<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
if ($this->session->flashdata('success'))
{
	echo "<script>alert('".$this->session->flashdata('success')."');</script>";
}
$this->load->view('template/6_footer.php');
?>
