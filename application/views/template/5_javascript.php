</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- DataTables js -->
<script src="<?= base_url('vendor/almasaeed2010/adminlte/bower_components/datatables.net/js/jquery.dataTables.js'); ?>"></script>
<script src="<?= base_url('vendor/almasaeed2010/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/fastclick/lib/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js'); ?>"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/jvectormap/jquery-jvectormap.js'); ?>"></script>
<script src="<?php site_url('http://jvectormap.com/js/jquery-jvectormap-1.2.2.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/chart.js/Chart.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/dist/js/demo.js'); ?>"></script>
<!-- Alertify JS -->
<script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/alertifyjs/build/alertify.js'); ?>"></script>

<script type="text/javascript">
	$(document).ready(function ()
	{
		var url = window.location;

		// for sidebar menu entirely but not cover treeview
		$('ul.sidebar-menu a').filter(function()
		{
			return this.href == url;
		}).parent().addClass('active');

		// for treeview
		$('ul.treeview-menu a').filter(function()
		{
			return this.href == url;
		}).closest('.treeview').addClass('active');
	});
</script>
