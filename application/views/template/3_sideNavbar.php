	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="<?php echo base_url('vendor/almasaeed2010/adminlte/dist/img/user2-160x160.jpg'); ?>" class="img-circle" alt="User Image">
				</div>
				<div class="pull-left info">
					<p>
						<?php echo $this->session->userdata('nama_lengkap'); ?>
					</p>
					<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
				</div>
			</div>

			<ul class="sidebar-menu" data-widget="tree">
				<li class="header">MAIN NAVIGATION</li>
				<?php
				$level     = $this->session->userdata('level');
				$main_menu = $this->db->get_where('tbl_menu', array('is_main_menu'=>0, 'level'=>$level));
				$uri2      = $this->uri->segment(2);
				
				echo '<li><a href="'. site_url($level.'/') .'"><i class="fa fa-home text-aqua"></i> Beranda</a></li>';

				foreach ($main_menu->result() as $main):
					$sub_menu = $this->db->get_where('tbl_menu', array('is_main_menu'=>$main->id_menu));
					if ($sub_menu->num_rows() > 0):
					
				?>
						<li class='treeview'></li>";
							<a href="<?php echo site_url($level."/".$main->link); ?>">
								<i class="fa fa-pencil"></i> <span> <?php echo $main->judul_menu; ?></span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
							<?php foreach ($sub_menu->result() as $sub): ?>
								<li <?php if ($uri2 == $sub->link) { echo "class='active'"; } ?>>
									<a href="<?php echo site_url($sub->link); ?>"><i class="fa <?php echo $sub->icon; ?>"></i> <?php echo $sub->judul_menu; ?></a>
								</li>
							<?php endforeach; ?>
							</ul>
						</li>					
				<?php 	else: ?>
						<!-- tanpa sub menu -->
						<li><a href="<?php echo site_url($level."/".$main->link); ?>"><i class="text-aqua fa <?php echo $main->icon; ?>"></i> <?php echo ucwords($main->judul_menu); ?></a></li>
				<?php
						endif;
				endforeach;
				?>
				<li><a href="<?php echo site_url('welcome/logout'); ?>"><i class="fa fa-sign-out text-aqua"></i> Sign Out</a></li>
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
