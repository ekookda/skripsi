</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<header class="main-header">
			<!-- Logo -->
			<a href="#" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>A</b>LT</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b><?=ucfirst(strtolower($this->session->userdata('level')));?></b>LTE</span>
			</a>

			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a> -->
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo base_url('vendor/almasaeed2010/adminlte/dist/img/user2-160x160.jpg'); ?>" class="user-image" alt="User Image">
								<span class="hidden-xs"><?php echo "Hi, " . $this->session->userdata('nama_lengkap'); ?></span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="<?php echo base_url('vendor/almasaeed2010/adminlte/dist/img/user2-160x160.jpg'); ?>" class="img-circle" alt="User Image">

									<p>
										<?php echo $this->session->userdata('nama_lengkap'). " - " . ucfirst(strtolower($this->session->userdata('level'))); ?>
										<small>
											<?php 
											$table = ($this->session->userdata('level') == 'siswa') ? 'tbl_siswa' : 'tbl_users';
											$id    = ($this->session->userdata('level') == 'siswa') ? 'nisn' : 'id_users';
											$get   = $this->db->get_where($table, array($id=>$this->session->userdata('id')));
											
											$row = $get->result();
											echo "Member since " . date('M d', strtotime($row[0]->created_at));
											?>
										</small>
									</p>
								</li>

								<li class="user-footer">
									<div class="pull-left">
										<a href="<?php echo site_url($this->session->userdata('level').'/ubah_password');?>" class="btn btn-default btn-flat"><i class="fa fa-edit"></i> Password</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo site_url('welcome/logout');?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign Out</a>
									</div>
								</li>
							</ul>
						</li>
						
					</ul>
				</div>

			</nav>
		</header>
