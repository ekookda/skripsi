	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>&nbsp;</h1>
			<?php
			(!empty($content_header) ? $content_header : FALSE);
			// echo $content_header;
			$uri1 = $this->uri->segment(1);
			$uri2 = str_replace('-', ' ', $this->uri->segment(2));
			?>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
				<li class=""><?=ucfirst($uri1);?></li>
				<li class="active"><?=ucfirst($uri2);?></li>
			</ol>
		</section>
