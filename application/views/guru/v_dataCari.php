<?php
$this->load->view('template/1_header.php');
?>
<style>
	.error {
		color: red;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<?php 
			echo validation_errors(); 
			if ($this->session->flashdata('notif')): 
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php
				echo $this->session->flashdata('notif');
				echo validation_errors();
				?>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-plus-circle"></i> Input Nilai Peserta Didik</h3>
		</div>
		<!-- /.box-header -->
		
		<div class="box-body">
			<!-- form start -->
			<?php echo form_open('guru/input_nilai', ['class'=>'form-horizontal', 'id'=>'form_search']); ?>
				<div class="form-group">
					<?php
					echo form_label('Nomor Induk :', 'nisn', ['class'=>'col-sm-2 control-label']);
					$form_nisn = array(
						'type'     => 'number',
						'id'       => 'form_nisn',
						'name'     => 'nisn',
						'class'    => 'form-control',
						'required' => 'required'
					);
					?>
					<div class='col-sm-3'>
					<!-- <select name="nisn" class="form-control">
						<option value="">-- Data Siswa --</option>
						<?php foreach ($data->result() as $r): ?>
						<option value="<?=$r->nisn;?>"><?=$r->nisn." - ".$r->nama_lengkap;?></option>
						<?php endforeach; ?>
					</select> -->
						<?php echo form_input($form_nisn); ?>
					</div>
					<?php echo form_submit('btn_cari', 'Cari', ['class'=>'btn btn-primary']); ?>
				</div> <!-- /.form-group -->
			<?php echo form_close(); ?>
		</div> <!-- /.box-body -->

		<div class="box-footer"></div>
		<!-- /.box-footer -->	
		
	</div>
	<!-- /.box-primary -->
</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);
	
	$("#level").change(function() {
		var level = $(this).val();
		if (level != '')
		{
			// alert(level);
			$.get("get_id_users/" + level, function(data, status) {
				console.log(data);
				// console.log(status);
				$(".id_users").val(data);
			});
		}
		else
		{
			$(".id_users").val('');
		}
	});
	
});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
