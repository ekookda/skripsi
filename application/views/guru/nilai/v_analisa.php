<?php
$this->load->view('template/1_header.php');
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>
<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-sm-12">
			<?php if ($this->session->flashdata('notif')): ?>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?= $this->session->flashdata('notif'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<!-- /.box -->

    <!-- Default box -->
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-calculator"> <?= $box_title; ?></i></h3>
		</div>

		<div class="box-body">
			<div class="table-responsive">
				<!-- Table data bobot kriteria -->
				<?php
				$no = 1;
				if (isset($db_bobot)) :
					echo form_fieldset('Nilai Bobot Kriteria');
				?>
					<table id="example1" class="table table-striped table-hover table-condensed">
						<thead>
						<tr>
							<th class="text-center">Kriteria</th>
							<th class="text-center">K1<br>Minat Siswa</th>
							<th class="text-center">K2<br>Minat Orang Tua</th>
							<th class="text-center">K3<br>Minat Mapel</th>
							<th class="text-center">K4<br>Nilai Rapor</th>
							<th class="text-center">K5<br>Nilai UN/US</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td class="text-left">Bobot Awal</td>
							<?php foreach ($db_bobot->result() as $row): ?>
								<td class="text-center"><?= $row->bobot_awal; ?></td>
							<?php endforeach; ?>
						</tr>
						<tr>
							<td class="text-left">Bobot Perbaikan</td>
							<?php foreach ($db_bobot->result() as $row): ?>
								<td class="text-center"><?= $row->bobot_perbaikan; ?></td>
							<?php endforeach; ?>
						</tr>
						</tbody>
					</table>
				<?php
				else:
				?>
					<table id="example1" class="table table-striped table-hover table-condensed">
					<thead>
						<tr>
							<th class="text-center">Kriteria</th>
							<th class="text-center">K1<br>Minat Siswa</th>
							<th class="text-center">K2<br>Minat Orang Tua</th>
							<th class="text-center">K3<br>Minat Mapel</th>
							<th class="text-center">K4<br>Nilai Rapor</th>
							<th class="text-center">K5<br>Nilai UN/US</th>
						</tr>
						</thead>
						<tbody></tbody>
					</table>
				<?php
					echo form_fieldset_close();
				endif;
				?>
			</div>
			<!-- /.table-responsive -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			<?php
			echo form_open('guru/perhitungan');
			echo form_submit('btn_hitung', 'Analisa', ['class'=>'btn btn-primary']); 
			echo form_close();
			?>
		</div>
		<!-- /.box-footer-->
	</div>

	<?php if (isset($db_alternatif)): ?>
	<div class="row">
		<!-- IPA/MIPA -->
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-info-circle"></i> Alternatif MIPA</h3>
				</div>
				<!-- /.box-header -->
			
				<!-- table keterangan konversi nilai -->
				<div class="box-body">
					<div class="table-responsive">
						<table id="alt_mipa" class="table table-hover table-condensed table-bordered">
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Nama Peserta Didik</th>
								<th class="text-center">K1</th>
								<th class="text-center">K2</th>
								<th class="text-center">K3</th>
								<th class="text-center">K4</th>
								<th class="text-center">K5</th>
							</tr>
							</thead>
							<tbody>
							<?php $no=1; foreach ($db_alternatif->result() as $row): ?>
								<tr>
									<td class="text-center"><?= $no++; ?></td>
									<td><?= $row->nama_lengkap; ?></td>
									<td><?= $row->k1_mipa; ?></td>
									<td><?= $row->k2_mipa; ?></td>
									<td><?= $row->k3_mipa; ?></td>
									<td><?= $row->k4_mipa; ?></td>
									<td><?= $row->k5_mipa; ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<!-- end table keterangan konversi nilai -->
				</div>
				<!-- /.box-body -->

				<div class="box-footer"></div>
				<!-- /.box-footer -->	
			</div>
			<!-- /.box-primary -->
		</div>
		<!-- /.col-md-6 -->

		<!-- IIS/IPS -->
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-info-circle"></i> Alternatif IPS</h3>
				</div>
				<!-- /.box-header -->
			
				<!-- table keterangan konversi nilai -->
				<div class="box-body">
					<div class="table-responsive">
						<table id="alt_mipa" class="table table-hover table-condensed table-bordered">
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Nama Peserta Didik</th>
								<th class="text-center">K1</th>
								<th class="text-center">K2</th>
								<th class="text-center">K3</th>
								<th class="text-center">K4</th>
								<th class="text-center">K5</th>
							</tr>
							</thead>
							<tbody>
							<?php $no=1; foreach ($db_alternatif->result() as $row): ?>
								<tr>
									<td class="text-center"><?= $no++; ?></td>
									<td><?= $row->nama_lengkap; ?></td>
									<td><?= $row->k1_iis; ?></td>
									<td><?= $row->k2_iis; ?></td>
									<td><?= $row->k3_iis; ?></td>
									<td><?= $row->k4_iis; ?></td>
									<td><?= $row->k5_iis; ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<!-- end table keterangan konversi nilai -->
				</div>
				<!-- /.box-body -->

				<div class="box-footer"></div>
				<!-- /.box-footer -->	
			</div>
			<!-- /.box-primary -->
		</div>
		<!-- /.col-md-6 -->
	</div>
	<!-- /.row -->
	<?php endif; ?>

	<!-- Default box -->
	<div class="box box-success">
		<div class="box-body">
			<div class="table-responsive">
				<!-- Table data bobot kriteria -->
				<?php
				$no = 1;
				echo form_fieldset('Hasil Perhitungan');
				?>
				<table id="example1" class="table table-striped table-hover table-condensed">
					<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">NISN</th>
						<th class="text-center">Nama Peserta Didik</th>
						<th class="text-center">V MIPA</th>
						<th class="text-center">V IIS</th>
						<th class="text-center">Prioritas 1</th>
						<th class="text-center">Prioritas 2</th>
						<th class="text-center">Detail</th>
					</tr>
					</thead>
					<tbody>
					<?php 
					if (isset($db_hasil)):
						if ($db_hasil->num_rows() > 0):
							$no = 1;
							foreach ($db_hasil->result() as $alt):
						?>
							<tr>
								<td class="text-center"><?= $no;?></td>
								<td class="text-center"><?= $alt->nisn; ?></td>
								<td><?= $alt->nama_lengkap; ?></td>
								<td class="text-center"><?= $alt->v_mipa; ?></td>
								<td class="text-center"><?= $alt->v_iis; ?></td>
								<td class="text-center"><?= $alt->jurusan_prioritas; ?></td>
								<?php $jurusan_kedua = ($alt->jurusan_prioritas == "MIPA" ? "IIS" : "MIPA"); ?>
								<td class="text-center"><?= $jurusan_kedua; ?></td>
								<td class="text-center">
									<a href="<?= site_url('guru/detail/'.$alt->nisn); ?>" class="btn btn-link" data-toggle="tooltip" title="Lihat Detail!">
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
								</td>
							</tr>
						<?php 
								$no++;
							endforeach; 
						endif; 
					endif; 
					?>
					</tbody>
				</table>
			</div>
			<!-- /.table-responsive -->
		</div>
		<!-- /.box-body -->

		<div class="box-footer"></div>
		<!-- /.box-footer-->
	</div>

</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function()
{
	$('[data-toggle="tooltip"]').tooltip();

	$('.table').dataTable({
		"ordering" : false,
		"info" : false,
		"searching" : false,
		"paging" : false
	});

	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);

});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
