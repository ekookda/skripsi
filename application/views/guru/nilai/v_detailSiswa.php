<?php $this->load->view('template/1_header.php'); ?>
<style>
	fieldset.scheduler-border
	{
		border: 1px groove #ddd !important;
		padding: 0 1.4em 1.4em 1.4em !important;
		margin: 0 0 1.5em 0 !important;
		-webkit-box-shadow: 0px 0px 0px 0px #000;
				box-shadow: 0px 0px 0px 0px #000;
	}

	legend.scheduler-border
	{
		font-size: 1.2em !important;
		font-weight: bold !important;
		text-align: left !important;
		width: auto;
		padding: 0 10px;
		border-bottom: none;
	}

	legend
	{
		font-size: 1.2em !important;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>
<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-sm-12">

		<?php if ($this->session->flashdata('notif') || validation_errors()): ?>

			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?= $this->session->flashdata('notif'); ?>
				<?= validation_errors(); ?>
			</div>

		<?php elseif ($this->session->flashdata('success')): ?>

			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?= $this->session->flashdata('success'); ?>
			</div>

		<?php endif; ?>

		</div>
	</div>

    <!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-info-circle"></i> <?= $box_title; ?></h3>
		</div>  <!-- /.box-header -->

		<div class="box-body">
		<?php 
		if ($data->num_rows() > 0): 
			$data = $data->row(); 
		?>
			<div class="table-responsive">
				<div class="row">
					<div class="col-sm-6">
					<?php echo form_fieldset('Detail Siswa', ['class'=>'scheduler-border']); ?>
						<table class="table table-hover table-condensed">
							<tr>
								<th width="225px">NISN</th>
								<td width="10px">:</td>
								<td><?php echo $data->nisn; ?></td>
							</tr>
							<tr>
								<th>Nama Peserta Didik</th>
								<td>:</td>
								<td><?php echo $data->nama_lengkap; ?></td>
							</tr>
							<tr>
								<th>Asal Sekolah</th>
								<td>:</td>
								<td><?php echo $data->asal_sekolah; ?></td>
							</tr>
						</table>
					<?php echo form_fieldset_close(); ?>
					</div>
					<div class="col-sm-6">
					<?php echo form_fieldset('Pilihan Angket', ['class'=>'scheduler-border']); ?>
						<table class="table table-hover table-condensed">
						<?php
							$kriteria1 = ($data->k1_mipa == 2) ? "MIPA" : "IIS";
							$kriteria2 = ($data->k2_mipa == 2) ? "MIPA" : "IIS";
						?>
							<tr>
								<th width="225px">Peminatan Jurusan</th>
								<td width="10px">:</td>
								<td>
								<?php echo $kriteria1; ?>
								</td>
							</tr>
							<tr>
								<th>Pilihan Orang Tua</th>
								<td>:</td>
								<td><?php echo $kriteria2; ?></td>
							</tr>
							<tr>
								<th>Mata Pelajaran Pilihan</th>
								<td>:</td>
								<td><?php echo $data->minat_mapel; ?></td>
							</tr>
						</table>
					<?php echo form_fieldset_close(); ?>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-6">
					<?php echo form_fieldset('Detail Nilai', ['class'=>'scheduler-border']); ?>
						<table class="table table-hover table-condensed">
							<tr>
								<th width="225px">Nilai Rapor Rata-rata IPA</th>
								<td width="10px">:</td>
								<td><?php echo $data->rata_ipa; ?></td>
							</tr>
							<tr>
								<th>Nilai Rapor Rata-rata IPS</th>
								<td>:</td>
								<td><?php echo $data->rata_ips; ?></td>
							</tr>
							<tr>
								<th>Nilai UN IPA</th>
								<td>:</td>
								<td><?php echo $data->un_ipa; ?></td>
							</tr>
							<tr>
								<th>Nilai US IPS</th>
								<td>:</td>
								<td><?php echo $data->us_ips; ?></td>
							</tr>
						</table>
					<?php echo form_fieldset_close(); ?>
					</div>
					<div class="col-sm-6">
					<?php echo form_fieldset('Rekomendasi Jurusan', ['class'=>'scheduler-border']); ?>
						<table class="table table-hover table-condensed">
							<?php
							if ($data->minat_jurusan == "MIPA")
							{
								$prioritas1 = "MIPA";
								$prioritas2 = "IIS";
							}
							else
							{
								$prioritas1 = "IIS";
								$prioritas2 = "MIPA";
							}
							?>
							<tr>
								<th width="225px">Prioritas 1</th>
								<td width="10px">:</td>
								<td><?php echo $prioritas1; ?></td>
							</tr>
							<tr>
								<th>Prioritas 2</th>
								<td>:</td>
								<td><?php echo $prioritas2; ?></td>
							</tr>
						</table>
					<?php echo form_fieldset_close(); ?>
					</div>
				</div>
			</div>
		<?php else: ?>
			<h3 class="box-title text-center">Belum ada hasil !</h3>
		<?php endif; ?>
		</div> <!-- /.box-body -->
	</div> <!-- /.box -->

</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	$('#example1').dataTable({
		
	});

	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);

});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
