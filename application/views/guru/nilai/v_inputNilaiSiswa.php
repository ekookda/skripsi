<?php
$this->load->view('template/1_header.php');
?>
<style>
	.error {
		color: red;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<?php
			if ($this->session->flashdata('notif')): 
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php
				echo $this->session->flashdata('notif');
				echo validation_errors();
				?>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-plus-circle"></i> Input Nilai Peserta Didik</h3>
		</div>
		<!-- /.box-header -->
		
		<div class="box-body">
		<?php # print_r($data->row()); exit; ?>
			<?php foreach($data->result() as $r): ?>
				<div class="form-horizontal">
					<div class="form-group">
						<?php echo form_label('NISN :', 'nisn', ['class'=>'col-sm-2 control-label']); ?>
						<div class="col-sm-3">
							<?php echo form_input('nisn', $r->nisn, ['class'=>'form-control', 'disabled'=>'disabled']); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo form_label('Nama Peserta Didik :', 'nama_siswa', ['class'=>'col-sm-2 control-label']); ?>
						<div class="col-sm-3">
							<?php echo form_input('nama_siswa', $r->nama_lengkap, ['class'=>'form-control', 'disabled'=>'disabled']); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo form_label('Asal Sekolah :', 'asal_sekolah', ['class'=>'col-sm-2 control-label']); ?>
						<div class="col-sm-3">
							<?php echo form_input('asal_sekolah', $r->asal_sekolah, ['class'=>'form-control', 'disabled'=>'disabled']); ?>
						</div>
					</div>
				</div>

				<br>

				<div class="table-responsive">
				<?php echo form_open('guru/edit_nilai/'.$r->nisn, ['role'=>'form'], ['nisn'=>$r->nisn]); ?>
					<table id="tbl_nilai" class="table table-condensed table-hover">
						<thead>
						<tr>
							<td></td>
							<?php
							$awal  = 1;
							$akhir = 6;
							while ($awal <= $akhir)
							{
								echo "<th class='text-center'>Semester ". $awal ."</th>";
								$awal++;
							}
							?>
							<th class='text-center'>UN/US</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<th width="10%">IPA</th>
							<td><?php echo form_input('ipasm1', ($r->ipasm1 < 1) ? set_value('ipasm1') : $r->ipasm1, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipasm2', ($r->ipasm2 < 1) ? set_value('ipasm2') : $r->ipasm2, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipasm3', ($r->ipasm3 < 1) ? set_value('ipasm3') : $r->ipasm3, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipasm4', ($r->ipasm4 < 1) ? set_value('ipasm4') : $r->ipasm4, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipasm5', ($r->ipasm5 < 1) ? set_value('ipasm5') : $r->ipasm5, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipasm6', ($r->ipasm6 < 1) ? set_value('ipasm6') : $r->ipasm6, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('un_ipa', ($r->un_ipa < 1) ? set_value('un_ipa') : $r->un_ipa, ['class'=>'form-control']); ?></td>
						</tr>
						<tr>
							<th >IPS</th>
							<td><?php echo form_input('ipssm1', ($r->ipssm1 < 1) ? set_value('ipssm1') : $r->ipssm1, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipssm2', ($r->ipssm2 < 1) ? set_value('ipssm2') : $r->ipssm2, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipssm3', ($r->ipssm3 < 1) ? set_value('ipssm3') : $r->ipssm3, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipssm4', ($r->ipssm4 < 1) ? set_value('ipssm4') : $r->ipssm4, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipssm5', ($r->ipssm5 < 1) ? set_value('ipssm5') : $r->ipssm5, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('ipssm6', ($r->ipssm6 < 1) ? set_value('ipssm6') : $r->ipssm6, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('us_ips', ($r->us_ips < 1) ? set_value('us_ips') : $r->us_ips, ['class'=>'form-control']); ?></td>
						</tr>
						<tr>
							<th >Matematika</th>
							<td><?php echo form_input('mtksm1', ($r->mtksm1 < 1) ? set_value('mtksm1') : $r->mtksm1, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('mtksm2', ($r->mtksm2 < 1) ? set_value('mtksm2') : $r->mtksm2, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('mtksm3', ($r->mtksm3 < 1) ? set_value('mtksm3') : $r->mtksm3, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('mtksm4', ($r->mtksm4 < 1) ? set_value('mtksm4') : $r->mtksm4, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('mtksm5', ($r->mtksm5 < 1) ? set_value('mtksm5') : $r->mtksm5, ['class'=>'form-control']); ?></td>
							<td><?php echo form_input('mtksm6', ($r->mtksm6 < 1) ? set_value('mtksm6') : $r->mtksm6, ['class'=>'form-control']); ?></td>
							<td></td>
						</tr>
						</tbody>
						<tfoot>
						<tr>
						<td>&nbsp;</td>
						<td colspan="2">
						<?php
							echo form_submit('btn_update', 'Simpan', ['class'=>'btn btn-primary', 'id'=>'btn_update']);
							echo form_button('btn_batal', 'Batal', ['class'=>'btn btn-danger', 'id'=>'btn_batal', 'onClick'=>'javascript: history.go(-1);']); 
						?>
						</td>
						</tr>
						</tfoot>
					</table>
				<?php echo form_close(); ?>
				</div> <!-- /.table-responsive -->
			<?php endforeach; ?>
		</div> <!-- /.box-body -->

		<div class="box-footer">
			<h5><strong>NOTE :</strong> 
				<ul>
					<li>Penulisan nilai disertai 2 angka dibelakang koma, misal: 79.00</li>
					<li>Penulisan tanda pemisah koma menggunakan tanda titik</li>
				</ul>
			</h5>
		</div>
		<!-- /.box-footer -->	
		
	</div>
	<!-- /.box-primary -->
</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);
	
	$("#level").change(function() {
		var level = $(this).val();
		if (level != '')
		{
			// alert(level);
			$.get("get_id_users/" + level, function(data, status) {
				console.log(data);
				// console.log(status);
				$(".id_users").val(data);
			});
		}
		else
		{
			$(".id_users").val('');
		}
	});
	
});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
