<?php
$this->load->view('template/1_header.php');
?>
<style>
	.error {
		color: red;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<?php
			if ($this->session->flashdata('notif')): 
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $this->session->flashdata('notif'); ?>
			</div>
			<?php elseif ($this->session->flashdata('success')): ?>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-plus-circle"></i> Input Nilai Bobot</h3>
				</div>
				<!-- /.box-header -->
			
				<div class="box-body">
					<?php if (validation_errors()): ?>
						<div class="alert alert-danger" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<?php echo validation_errors(); ?>
						</div>
					<?php endif; ?>

					<!-- form start -->
					<?php 
					echo form_open('bobot/save', ['class'=>'form-horizontal', 'id'=>'form_bobot']);
					echo form_fieldset('Bobot Kriteria');

					$nilai = [
						'' => '-- Pilih nilai bobot --',
						1 => 1,
						2 => 2,
						3 => 3,
						4 => 4,
						5 => 5
					];
					?>

					<div class="table-responsive col-sm-12">
						<table class="table table-striped table-condensed table-hover">
							<thead>
							<tr>
								<th class="text-center">Kriteria</th>
								<th class="text-center">K1<br>Minat Siswa</th>
								<th class="text-center">K2<br>Minat Orang Tua</th>
								<th class="text-center">K3<br>Minat Mapel</th>
								<th class="text-center">K4<br>Nilai Rapor</th>
								<th class="text-center">K5<br>Nilai UN/US</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td class="text-center">Nilai Kepentingan</td>
								<?php for ($i=1; $i < count($nilai); $i++): ?>
								<td>
									<?php echo form_dropdown('kriteria'.$i, $nilai, '', ['class'=>'form-control']); ?>
								</td>
								<?php endfor; ?>
							</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="6">
									<?php echo form_submit('btn_nilai', 'Simpan Data', ['class'=>'btn btn-primary pull-left', 'onclick'=>'return confirm(\'Yakin dengan nilai yang dimasukkan ?\')']); ?>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>

					<?php
					echo form_fieldset_close();
					echo form_close(); 
					?>
					<!-- form end -->
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box-primary -->
		</div>
		<!-- /.col-md-6 -->
	</div>
	<!-- /.row -->

	<div class="row">
		<div class="col-sm-8">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-info-circle"></i> Data Nilai Bobot</h3>
				</div>
				<!-- /.box-header -->
			
				<!-- table keterangan konversi nilai -->
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-hover table-condensed table-striped">
							<thead>
							<tr>
								<th class="text-center">Kepentingan</th>
								<th class="text-center">K1<br>Minat Siswa</th>
								<th class="text-center">K2<br>Minat Orang Tua</th>
								<th class="text-center">K3<br>Minat Mapel</th>
								<th class="text-center">K4<br>Nilai Rapor</th>
								<th class="text-center">K5<br>Nilai UN/US</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td class="text-left">Bobot Awal</td>
							<?php 
							if (isset($data)): 
								foreach ($data as $row):
							?>
									<td class="text-center"><?= $row->bobot_awal; ?></td>
							<?php
								endforeach;
							else:
								echo "<td colspan='5'>Nilai bobot kriteria belum dibuat</td>";
							endif; 
							?>
							</tr>
							<tr>
							<td class="text-left">Bobot Perbaikan</td>
							<?php 
							if (isset($data)): 
								$total_bobot = 0;
								foreach ($data as $row):
							?>
									<td class="text-center"><?= $row->bobot_perbaikan; ?></td>
							<?php
								$total_bobot += $row->bobot_perbaikan;
								endforeach;
							else:
								echo "<td colspan='5'>Nilai bobot kriteria belum dibuat</td>";
							endif; 
							?>
							</tr>
							<tr>
							<th>Total Bobot</th>
							<?php echo "<td colspan='' class='text-center'><strong>".floor($total_bobot)."</strong></td>"; ?>
							<td colspan="4"></td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box-primary -->
		</div>

		<div class="col-md-4">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-info-circle"></i> Konversi Nilai Bobot</h3>
				</div>
				<!-- /.box-header -->
			
				<!-- table keterangan konversi nilai -->
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-hover table-condensed table-striped">
							<thead>
							<tr>
								<th class="text-center">Nilai/Angka</th>
								<th class="text-center">Keterangan</th>
							</tr>
							</thead>
							<tbody>
							<?php 
							$no=1;
							$konversi = [
								'1' => 'Sangat Tidak Penting',
								'2' => 'Tidak Penting',
								'3' => 'Penting',
								'4' => 'Cukup Penting',
								'5' => 'Sangat Penting'
							];
							foreach ($konversi as $n => $ket): ?>
								<tr>
									<td class="text-center"><?= $n; ?></td>
									<td class="text-center"><?= $ket; ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<!-- end table keterangan konversi nilai -->
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box-primary -->
		</div>
		<!-- /.col-md-4 -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);	
});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
