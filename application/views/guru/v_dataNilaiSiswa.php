<?php
$this->load->view('template/1_header.php');
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>
<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-sm-12">
			<?php if ($this->session->flashdata('notif')): ?>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?= $this->session->flashdata('notif'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>

    <!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= $box_title; ?></h3>
		</div>
		<div class="box-body">
			<div class="">
				<a class="btn btn-primary" href="<?= site_url('admin/tambah-siswa'); ?>"><i class="fa fa-plus"></i> Tambah Data</a>
				<br><br>
			</div>
			<div class="table-responsive">
			<?php $no=1; if ($row != FALSE): ?>
					<table id="example1" class="table table-striped table-hover table-condensed">
						<thead>
						<tr>
							<th class="text-center">No.</th>
							<th class="text-center">NISN</th>
							<th class="text-center">Nama Lengkap</th>
							<th class="text-center">Tempat/Tgl. Lahir</th>
							<th class="text-center">Alamat</th>
							<th class="text-center">Asal Sekolah</th>
							<th class="text-center">Tahun Ajaran</th>
							<th class="text-center"></th>
						</tr>
						</thead>
						<tbody>
			<?php foreach ($row as $r): ?>
							<tr>
								<td class="text-center"><?=$no++;?></td>
								<td class="text-center">
									<a href="<?= site_url('admin/detail/'.$r->nisn); ?>"><?= $r->nisn; ?></a>
								</td>
								<td><?=$r->nama_lengkap;?></td>
								<td><?=$r->tempat_lahir.", ".date('d-m-Y', strtotime($r->tgl_lahir));?></td>
								<td><?=$r->alamat;?></td>
								<td class="text-center"><?=$r->asal_sekolah;?></td>
								<td class="text-center"><?=$r->tahun_ajaran;?></td>
								<td class="text-center">
									<a type="button" href="<?php echo site_url('admin/delete-siswa/'.$r->nisn); ?>" id="btn-hapus" class="btn btn-xs btn-danger" onclick="return confirm('Yakin ingin menghapus ?')"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
			<?php endforeach; ?>
						</tbody>
					</table>
			<?php
			else:
				echo $row;
			endif;
			?>
			</div>
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			
		</div>
		<!-- /.box-footer-->
	</div>
	<!-- /.box -->

</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	$('#example1').dataTable({
		
	});

	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);

});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
