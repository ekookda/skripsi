<?php require_once('template/1_header.php'); ?>

	<!-- iCheck -->
	<link rel="stylesheet" href="<?= base_url('vendor/almasaeed2010/adminlte'); ?>/plugins/iCheck/square/blue.css">

	<style>
		#notif 
		{
			color: red;
		}
	</style>

</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="#"><b>Peminatan</b> Jurusan</a>
	</div>
	<!-- /.login-logo -->
	<div class="login-box-body">
		<p class="login-box-msg text-danger"><?= $this->session->flashdata('notif'); ?></p>
		<p class="login-box-msg text-danger"><?= validation_errors(); ?></p>

		<!-- Form Open -->
		<?= form_open('siswa/validasi_login', array('class'=>'form', 'id'=>'form_login')); ?>

		<div class="form-group has-feedback">
			<?= form_input('nisn', '', array('id'=>'nisn', 'class'=>'form-control', 'placeholder'=>'Nomor Induk Siswa Nasional')); ?>
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>

		<div class="form-group has-feedback">
			<?= form_password('password', '', array('id'=>'password', 'class'=>'form-control', 'placeholder'=>'Password')); ?>
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>

		<div class="row">
			<div class="col-xs-8">
			<div class="checkbox icheck">
				<label>
				<input type="checkbox"> Ingatkan saya
				</label>
			</div>
			</div>
			<!-- /.col -->
			<div class="col-xs-4">
				<?= form_submit('', 'Masuk', array('class'=>'btn btn-primary btn-block btn-flat')); ?>
			</div>
			<!-- /.col -->
		</div>

		<?= form_close(); ?>
		<!-- Form Closed -->

	</div>
	<!-- /.login-box-body -->

<?php require_once('template/5_javascript.php'); ?>

<!-- iCheck -->
<script src="<?= base_url('vendor/almasaeed2010/adminlte'); ?> /plugins/iCheck/icheck.min.js"></script>
<script>
	$(function () {
		$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' /* optional */
		});
	});
</script>
</body>
</html>
