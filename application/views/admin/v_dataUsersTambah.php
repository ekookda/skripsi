<?php
$this->load->view('template/1_header.php');
?>
<style>
	.error {
		color: red;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<?php if ($this->session->flashdata('notif')): ?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $this->session->flashdata('notif'); ?>
			</div>
			<?php elseif ($this->session->flashdata('success')): ?>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-plus-circle"></i> Tambah Users</h3>
		</div>
		<!-- /.box-header -->
		
		<!-- form start -->
		<?php echo form_open('admin/validate_users', array('class'=>'form', 'role'=>'form')); ?>
			<!-- row -->
			<div class="row">
				<!-- general form elements -->
				<div class="col-sm-6">
					<div class="box-body">
						<div class="form-group">
							<?php
							echo form_label('ID Users', 'id_users', array('for'=>'id_users'));
							$attribute = array(
								'type' => 'text',
								'class' => 'form-control id_users',
								'name' => 'id_users',
								'id' => 'form_id_users',
								'max' => '5',
								'readonly' => 'readonly'
							);
							echo form_input($attribute);
							echo "<span class='error'>".form_error('id_users')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Username', 'username');
							echo form_input('username', set_value('username'), array('class'=>'form-control', 'id'=>'form_username', 'placeholder'=>'Masukkan Username'));
							echo "<span class='error'>".form_error('username')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Password', 'password');
							echo form_password('password', '', array('class'=>'form-control', 'id'=>'form_password', 'placeholder'=>'Masukkan Password'));
							echo "<span class='error'>".form_error('password')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Nama Lengkap', 'nama_lengkap');
							echo form_input('nama_lengkap', set_value('nama_lengkap'), array('class'=>'form-control', 'id'=>'form_nama_lengkap', 'placeholder'=>'Masukkan Nama Lengkap'));
							echo "<span class='error'>".form_error('nama_lengkap')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Level', 'level');
							$tahun_sekarang = date('Y');
							?>
							<select class="form-control" name="level" id="level" required="required">
								<?php 
									$list_level = array(
										''		=> '-- Level Akses --',
										'ADMIN' => 'Admin',
										'GURU'  => 'Guru'
									);
									foreach ($list_level as $level => $data_level):										
								?>
										<option value="<?php echo $level; ?>"><?php echo $data_level; ?></option>
								<?php
									endforeach;
								?>
							</select>
							<?php echo "<span class='error'>".form_error('level')."</span>"; ?>
						</div>
						<?php
							$attribute = array(
								'type' => 'hidden',
								'class' => 'id_users',
								'name' => 'kd_users',
								'id' => 'form_kd_users'
							);
							echo form_input($attribute);
						?>
						<div class="box-footer">
							<input type="submit" class="btn btn-primary" name="btn_submit" value="Simpan" />
							<button type="reset" class="btn btn-danger" name="btn-reset">Cancel</button>
						</div>
					</div>
					<!-- box-body -->
				</div>
				<!-- /.col-md-6 -->
			</div>
			<!-- /.row -->
		<?php echo form_close(); ?>
	</div>
	<!-- /.box-primary -->
</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);
	
	$("#level").change(function() {
		var level = $(this).val();
		if (level != '')
		{
			// alert(level);
			$.get("get_id_users/" + level, function(data, status) {
				console.log(data);
				// console.log(status);
				$(".id_users").val(data);
			});
		}
		else
		{
			$(".id_users").val('');
		}
	});
	
});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
