<?php
$this->load->view('template/1_header.php');
?>
<style>
	.error {
		color: red;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<?php 
			echo validation_errors(); 
			if ($this->session->flashdata('notif')): 
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php
				echo $this->session->flashdata('notif');
				echo validation_errors();
				?>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-plus-circle"></i> Ubah Data Peserta Didik</h3>
		</div>
		<!-- /.box-header -->
		
		<!-- form start -->
		<?php
		foreach ($data as $row):
			echo form_open('admin/edit-siswa/'.$row->nisn, array('class'=>'form', 'role'=>'form'));
		?>
				<!-- row -->
				<div class="row">
					<!-- general form elements -->
					<div class="col-sm-6">
						<div class="box-body">
							<div class="form-group">
								<?php
								echo form_label('NISN', 'nisn', array('for'=>'nisn'));
								$attribute = array(
									'type' => 'number',
									'class' => 'form-control',
									'name' => 'nisn',
									'id' => 'form_nisn',
									'placeholder' => 'Masukkan NISN',
									'max' => '9999999999',
									'value' => $row->nisn,
									'readonly' => 'readonly'
								);
								echo form_input($attribute);
								echo "<span class='error'>".form_error('nisn')."</span>";
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Nama Lengkap', 'nama_lengkap', array('for'=>'nama_lengkap'));
								echo form_input('nama_lengkap', $row->nama_lengkap, array('class'=>'form-control', 'id'=>'form_nama_lengkap', 'placeholder'=>'Masukkan Nama Lengkap'));
								echo "<span class='error'>".form_error('nama_lengkap')."</span>";
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Tempat Lahir', 'tempat_lahir', array('for'=>'tempat_lahir'));
								echo form_input('tempat_lahir', $row->tempat_lahir, array('class'=>'form-control', 'id'=>'form_tempat_lahir', 'placeholder'=>'Masukkan Tempat Lahir'));
								echo "<span class='error'>".form_error('tempat_lahir')."</span>";
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Tanggal Lahir', 'tgl_lahir', array('for'=>'tgl_lahir'));
								$date = array(
									'type' => 'date',
									'id' => 'id_tglLahir',
									'class' => 'form-control',
									'placeholder' => 'dd/mm/YY',
									'name' => 'tgl_lahir',
									'value' => $row->tgl_lahir
								);
								echo form_input($date);
								echo "<span class='error'>".form_error('tgl_lahir')."</span>";
								?>
							</div>
							<!-- /.box-body -->
					
						</div>
						<!-- box-body -->
					</div>
					<!-- /.col-md-6 -->
					
					<div class="col-sm-6">		
						<div class="box-body">
							<div class="form-group">
								<?php
								echo form_label('Alamat', 'alamat', array('for'=>'alamat'));
								$textarea = array(
									'id' => 'form_alamat',
									'name' => 'alamat',
									'class' => 'form-control',
									'placeholder' => 'Masukkan Alamat',
									'rows' => '5',
									'value' => $row->alamat
								);
								echo form_textarea($textarea);
								echo "<span class='error'>".form_error('alamat')."</span>";
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Asal Sekolah', 'asal_sekolah', array('for'=>'asal_sekolah'));
								echo form_input('asal_sekolah', $row->asal_sekolah, array('class'=>'form-control', 'id'=>'form_asal_sekolah', 'placeholder'=>'Masukkan Asal Sekolah'));
								echo "<span class='error'>".form_error('asal_sekolah')."</span>";
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Tahun Ajaran', 'tahun_ajaran', array('for'=>'tahun_ajaran'));
								$tahun_sekarang = date('Y');
								?>
								<select class="form-control" name="tahun_ajaran" id="tahun_ajaran" required="required">
									<option>-- Tahun Ajaran --</option>
									<?php 
										for ($i=$tahun_sekarang; $i < ($tahun_sekarang + 5); $i++):
										
									?>
											<option <?php if ($row->tahun_ajaran == $i."/".($i+1)) { echo 'selected="selected"'; } ?>><?php echo $i."/".($i+1); ?></option>
									<?php
										endfor;
									?>
								</select>
								<?php echo "<span class='error'>".form_error('tahun_ajaran')."</span>"; ?>
							</div>
							<!-- /.box-body -->
							
							<div class="box-footer">
								<input type="submit" class="btn btn-primary" name="btn_edit" value="Update" />
								<!-- <button type="reset" class="btn btn-danger" name="btn-reset">Reset</button> -->
							</div>
					
						</div>
						<!-- box-body -->
					</div>
					<!-- /.col-md-6 -->
				</div>
				<!-- /.row -->
		<?php
			echo form_close(); 
		endforeach;
		?>
	</div>
	<!-- /.box-primary -->
</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);
});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
