<?php
$this->load->view('template/1_header.php');
?>
<style>
	.error {
		color: red;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<?php 
			echo validation_errors(); 
			if ($this->session->flashdata('notif')): 
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php
				echo $this->session->flashdata('notif');
				echo validation_errors();
				?>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-plus-circle"></i> Ubah Data Peserta Didik</h3>
		</div>
		<!-- /.box-header -->
		
		<!-- form start -->
		<?php
		foreach ($data as $row):
			echo form_open('admin/edit-users/'.$row->id_users, array('class'=>'form', 'role'=>'form'), $row->id_users);
		?>
				<!-- row -->
				<div class="row">
					<!-- general form elements -->
					<div class="col-sm-6">
						<div class="box-body">
							<div class="form-group">
								<?php
								echo form_label('id_users', 'id_users', array('for'=>'id_users'));
								$attribute = array(
									'type' => 'text',
									'class' => 'form-control',
									'name' => 'id_users',
									'id' => 'form_id_users',
									'placeholder' => 'Masukkan id_users',
									'max' => '5',
									'value' => $row->id_users,
									'readonly' => 'readonly'
								);
								echo form_input($attribute);
								echo "<span class='error'>".form_error('id_users')."</span>";
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Username', 'username');
								echo form_input('username', $row->username, array('class'=>'form-control', 'id'=>'form_username', 'placeholder'=>'Masukkan Username'));
								echo "<span class='error'>".form_error('username')."</span>";
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Nama Lengkap', 'nama_lengkap');
								echo form_input('nama_lengkap', $row->nama_lengkap, array('class'=>'form-control', 'id'=>'form_nama_lengkap', 'placeholder'=>'Masukkan Nama Lengkap'));
								echo "<span class='error'>".form_error('nama_lengkap')."</span>";
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Level', 'level');
								$tahun_sekarang = date('Y');
								?>
								<select class="form-control" name="level" id="level" required="required" readonly="readonly">
									<option>-- Level Akses --</option>
									<?php 
										$list_level = array(
											'ADMIN' => 'Admin',
											'GURU'  => 'Guru'
										);
										foreach ($list_level as $level => $data_level):										
									?>
											<option <?php if ($row->level == $level) { echo 'selected="selected"'; } ?>><?php echo $data_level; ?></option>
									<?php
										endforeach;
									?>
								</select>
								<?php echo "<span class='error'>".form_error('level')."</span>"; ?>
							</div>
							
							<div class="box-footer">
								<input type="submit" class="btn btn-primary" name="btn_edit" value="Update" />
								<button type="reset" class="btn btn-danger" name="btn-reset">Cancel</button>
							</div>
						</div>
						<!-- box-body -->
					</div>
					<!-- /.col-md-6 -->
				</div>
				<!-- /.row -->
		<?php
			echo form_close(); 
		endforeach;
		?>
	</div>
	<!-- /.box-primary -->
</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);
});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
