<?php
$this->load->view('template/1_header.php');
?>
<style>
	.error {
		color: red;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>

<div class="row">
	<div class="col-sm-12">
		<?php if ($this->session->flashdata('notif')): ?>
		<div class="alert alert-success" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<?= $this->session->flashdata('notif'); ?>
		</div>
		<?php endif; ?>
	</div>
</div>

<!-- Main content -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-plus-circle"></i> Tambah Peserta Didik</h3>
		</div>
		<!-- /.box-header -->
		
		<!-- form start -->
		<?php
		echo form_open('admin/validate', array('class'=>'form', 'role'=>'form')); 
		?>
			<!-- row -->
			<div class="row">
				<!-- general form elements -->
				<div class="col-sm-6">
					<div class="box-body">
						<div class="form-group">
							<?php
							echo form_label('NISN', 'nisn', array('for'=>'nisn'));
							$attribute = array(
								'type'=>'number',
								'class'=>'form-control',
								'name'=>'nisn',
								'id'=>'form_nisn',
								'placeholder'=>'Masukkan NISN',
								'max'=>'9999999999',
								'value' => set_value('nisn')
							);
							echo form_input($attribute);
							echo "<span class='error'>".form_error('nisn')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Nama Lengkap', 'nama_lengkap', array('for'=>'nama_lengkap'));
							echo form_input('nama_lengkap', set_value('nama_lengkap'), array('class'=>'form-control', 'id'=>'form_nama_lengkap', 'placeholder'=>'Masukkan Nama Lengkap'));
							echo "<span class='error'>".form_error('nama_lengkap')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Tempat Lahir', 'tempat_lahir', array('for'=>'tempat_lahir'));
							echo form_input('tempat_lahir', set_value('tempat_lahir'), array('class'=>'form-control', 'id'=>'form_tempat_lahir', 'placeholder'=>'Masukkan Tempat Lahir'));
							echo "<span class='error'>".form_error('tempat_lahir')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Tanggal Lahir', 'tgl_lahir', array('for'=>'tgl_lahir'));
							$date = array(
								'type'=>'date',
								'id'=>'id_tglLahir',
								'class'=>'form-control',
								'placeholder'=>'dd/mm/YY',
								'name'=>'tgl_lahir',
								'value' => set_value('tgl_lahir')
							);
							echo form_input($date);
							echo "<span class='error'>".form_error('tgl_lahir')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Alamat', 'alamat', array('for'=>'alamat'));
							$textarea = array(
								'id' => 'form_alamat',
								'name' => 'alamat',
								'class' => 'form-control',
								'placeholder' => 'Masukkan Alamat',
								'rows' => '5',
								'value' => set_value('alamat')
							);
							echo form_textarea($textarea);
							echo "<span class='error'>".form_error('alamat')."</span>";
							?>
						</div>
						<!-- /.box-body -->
				
					</div>
					<!-- box-body -->
				</div>
				<!-- /.col-md-6 -->
				
				<div class="col-sm-6">		
					<div class="box-body">
						<div class="form-group">
							<?php
							echo form_label('Asal Sekolah', 'asal_sekolah', array('for'=>'asal_sekolah'));
							echo form_input('asal_sekolah', set_value('asal_sekolah'), array('class'=>'form-control', 'id'=>'form_asal_sekolah', 'placeholder'=>'Masukkan Asal Sekolah'));
							echo "<span class='error'>".form_error('asal_sekolah')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Password', 'password', array('for'=>'password'));
							echo form_password('password', '', array('class'=>'form-control', 'id'=>'form_password', 'placeholder'=>'Masukkan Password'));
							echo "<span class='error'>".form_error('password')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Tahun Ajaran', 'tahun_ajaran', array('for'=>'tahun_ajaran'));
							$tahun_sekarang = date('Y');
							?>
							<select class="form-control" name="tahun_ajaran" id="tahun_ajaran" required="required">
								<option>-- Tahun Ajaran --</option>
								<?php for ($i=$tahun_sekarang; $i<=($tahun_sekarang + 5); $i++): ?>
								<option><?= $i ."/". ($i+1); ?></option>
								<?php endfor; ?>
							</select>
							<?= "<span class='error'>".form_error('tahun_ajaran')."</span>"; ?>
						</div>
						<!-- /.box-body -->
						
						<div class="box-footer">
							<button type="submit" class="btn btn-primary" name="btn-submit">Submit</button>
							<button type="reset" class="btn btn-danger" name="btn-reset">Reset</button>
						</div>
				
					</div>
					<!-- box-body -->
				</div>
				<!-- /.col-md-6 -->
			</div>
			<!-- /.row -->
		<?= form_close(); ?>
	</div>
	<!-- /.box-primary -->
</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);
});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
