<?php
	$pdf = new Pdf('P', 'mm', 'Letter', true, 'UTF-8', false);
	$pdf->SetTitle('Rekomendasi Peminatan');
	$pdf->SetHeaderMargin(30);
	$pdf->SetTopMargin(20);
	$pdf->setFooterMargin(20);
	$pdf->SetAutoPageBreak(true);
	$pdf->SetAuthor('Author');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->AddPage();
	$i=0;
	$html='<h3 align="center">Data Perangkingan Rekomendasi Peminatan</h3>
			
			<table cellspacing="1" bgcolor="#666666" cellpadding="2">
				<tr bgcolor="#ffffff">
					<th width="5%" align="center">No</th>
					<th width="15%" align="center">NISN</th>
					<th width="30%" align="center">Nama Lengkap</th>
					<th width="10%" align="center">Vektor MIPA</th>
					<th width="10%" align="center">Vektor IIS</th>
					<th width="15%" align="center">Rekomendasi 1</th>
					<th width="15%" align="center">Rekomendasi 2</th>
				</tr>';
	foreach ($hasil->result() as $row) 
		{
			if ($row->jurusan_prioritas == 'MIPA')
			{
				$prioritas1 = 'MIPA';
				$prioritas2 = 'IIS';
			}
			else
			{
				$prioritas1 = 'IIS';
				$prioritas2 = 'MIPA';
			}
			$i++;
			$html.='<tr bgcolor="#ffffff">
					<td align="center">'.$i.'</td>
					<td>'.$row->nisn.'</td>
					<td>'.$row->nama_lengkap.'</td>
					<td align="center">'.$row->v_mipa.'</td>
					<td align="center">'.$row->v_iis.'</td>
					<td align="center">'.$prioritas1.'</td>
					<td align="center">'.$prioritas2.'</td>
				</tr>';
		}
	$html.='</table>';
	$pdf->writeHTML($html, true, false, true, false, '');
	$pdf->Output('hasil.pdf', 'I');
