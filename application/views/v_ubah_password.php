<?php
$this->load->view('template/1_header.php');
?>
<style>
	.error {
		color: red;
	}
</style>
<?php
$this->load->view('template/2_topNavbar');
$this->load->view('template/3_sideNavbar.php');
$this->load->view('template/4a_main_header.php');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<?php if ($this->session->flashdata('notif')): ?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $this->session->flashdata('notif'); ?>
			</div>
			<?php elseif ($this->session->flashdata('success')): ?>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><i class="fa fa-plus-circle"></i> Ubah Password</h3>
		</div>
		<!-- /.box-header -->
		
		<!-- form start -->
		<?php echo form_open($this->session->userdata('level').'/change_password', array('class'=>'form', 'role'=>'form')); ?>
			<!-- row -->
			<div class="row">
				<!-- general form elements -->
				<div class="col-sm-6">
					<div class="box-body">
						<div class="form-group">
							<?php
							echo form_label('Masukkan Password Baru', 'new_password');
							echo form_password('new_password', '', array('class'=>'form-control', 'id'=>'form_password', 'placeholder'=>'Masukkan Password'));
							echo "<span class='error'>".form_error('new_password')."</span>";
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Konfirmasi Password', 'confirm_password');
							echo form_password('confirm_password', '', array('class'=>'form-control', 'id'=>'form_password', 'placeholder'=>'Masukkan Password'));
							echo "<span class='error'>".form_error('confirm_password')."</span>";
							?>
						</div>
						
						<div class="box-footer">
							<input type="submit" class="btn btn-primary" name="btn_submit" value="Simpan" />
							<button type="reset" class="btn btn-danger" name="btn-reset">Cancel</button>
						</div>
					</div>
					<!-- box-body -->
				</div>
				<!-- /.col-md-6 -->
			</div>
			<!-- /.row -->
		<?php echo form_close(); ?>
	</div>
	<!-- /.box-primary -->
</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

<?php
$this->load->view('template/4c_main_footer.php');
$this->load->view('template/5_javascript.php');
?>

<script type="text/javascript">
$(document).ready( function() {
	window.setTimeout(function() {
		$(".alert").fadeTo(500, 0).slideUp(500, function() {
			$(this).remove();
		});
	}, 4000);
	
	$("#level").change(function() {
		var level = $(this).val();
		if (level != '')
		{
			// alert(level);
			$.get("get_id_users/" + level, function(data, status) {
				console.log(data);
				// console.log(status);
				$(".id_users").val(data);
			});
		}
		else
		{
			$(".id_users").val('');
		}
	});
	
});
</script>

<?php $this->load->view('template/6_footer.php'); ?>
