<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
	private $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('m_admin');
		$this->data['title'] = 'Template Admin';
	}

	public function isLoggedIn()
	{
		$isLoggedIn   = $this->session->userdata('isLoggedIn');
		$isLevelAdmin = $this->session->userdata('level');

		if (!$isLoggedIn || $isLoggedIn == FALSE)
		{
			$this->session->set_flashdata('notif', 'Anda belum melakukan login!');
			redirect('welcome');
		}

		if ($isLevelAdmin !== 'admin')
		{
			$this->session->set_flashdata('notif', 'Anda belum melakukan login sebagai administrator!');
			redirect('welcome');
		}
	}

	public function index()
	{
		$this->data['content_header'] = '<h1>Peminatan Peserta Didik Dengan Metode <i>Weighted Product</i></h1>';
		$this->data['title_header'] = 'Peminatan Peserta Didik Dengan Metode <i>Weighted Product</i>';
		$this->data['box_body'] = ucwords('sistem pendukung keputusan peminatan peserta didik dengan menggunakan metode <i>Weighted Product</i>');
		
		$this->load->view('admin/v_index', $this->data); // template
	}

	public function ubah_password()
	{
		$this->load->view('v_ubah_password', $this->data);
	}

	public function change_password()
	{
		if ($this->input->post('btn_submit'))
		{
			$this->form_validation->set_rules('new_password', 'Password Baru', 'required|min_length[8]');
			$this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[new_password]');

			if ($this->form_validation->run() == FALSE)
			{
				$this->ubah_password();
			}
			else
			{
				$new_password  = $this->input->post('new_password');
				$pass['password'] = password_hash($new_password, PASSWORD_DEFAULT);

				$upd_pass = $this->m_admin->set_data_update('tbl_users', $pass, ['id_users'=>$this->session->userdata('id')]);

				$this->session->set_flashdata('notif', 'Password berhasil diubah. Silahkan login kembali !');
				redirect('welcome');
				
			}
		}
		else
		{
			$this->session->set_flashdata('notif', 'Error !');
			redirect('admin/ubah_password/'.$this->session->userdata('id'));
		}
	}

	public function laporan()
	{
		redirect('laporan/detail_laporan');
	}

// ------------------------------------------------------------------------

	// CRUD PESERTA DIDIK
	public function data_siswa()
	{
		$query = $this->m_admin->get_data('tbl_siswa');

		if ($query->num_rows() > 0)
		{
			$this->data['row'] = $query->result();
		}
		else
		{
			// Data kosong
			$this->data['row'] = FALSE;
		}
		$this->data['box_title'] = "<i class='fa fa-users'></i> Data Peserta Didik";

		$this->load->view('admin/v_dataSiswa', $this->data);
	}

	public function tambah_siswa()
	{	
		$this->load->view('admin/v_dataSiswaTambah', $this->data);
	}

	public function validate()
	{
		$rules = array(
			array(
				'field' => 'nisn',
				'label' => 'NISN',
				'rules' => 'required|max_length[9999999999]|is_unique[tbl_siswa.nisn]'
			),
			array(
				'field' => 'nama_lengkap',
				'label' => 'Nama',
				'rules' => 'required'
			),
			array(
				'field' => 'tempat_lahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required'
			),
			array(
				'field' => 'tgl_lahir',
				'label' => 'Tanggal Lahir',
				'rules' => 'required'
			),
			array(
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules' => 'required'
			),
			array(
				'field' => 'asal_sekolah',
				'label' => 'Asal Sekolah',
				'rules' => 'required'
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|min_length[6]'
			),
			array(
				'field' => 'tahun_ajaran',
				'label' => 'Tahun Ajaran',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($rules);
			
		if ($this->form_validation->run() == FALSE)
		{
			$this->tambah_siswa();
		}
		else
		{
			$input_array = array(
				'nisn'         => $this->input->post('nisn'),
				'nama_lengkap' => ucwords($this->input->post('nama_lengkap')),
				'tempat_lahir' => ucwords($this->input->post('tempat_lahir')),
				'tgl_lahir'    => $this->input->post('tgl_lahir'),
				'alamat'       => ucwords($this->input->post('alamat')),
				'asal_sekolah' => strtoupper($this->input->post('asal_sekolah')),
				'password'     => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'tahun_ajaran' => $this->input->post('tahun_ajaran'),
				'created_at'   => date('Y-m-d H:i:s'),
				'id_users'     => $this->session->userdata('id')
			);
			$insert_db     = $this->m_admin->set_data('tbl_siswa', $input_array);
			$ins_tbl_nilai = $this->m_admin->set_data('tbl_nilai', array('nisn'=>$input_array['nisn'], 'created_at'=>$input_array['created_at']));
			
			if ($insert_db)
			{
				$this->session->set_flashdata('notif', 'Data berhasil ditambahkan!');
				redirect('admin/tambah-siswa');
			}
			else
			{
				$this->session->set_flashdata('notif', 'Data berhasil ditambahkan!');
				redirect('welcome/admin/data-siswa');
			}
		}
	}
	
	public function edit_siswa($nisn)
	{
		if ($this->input->post('btn_edit'))
		{
			$rules = array(
				array(
					'field' => 'nisn',
					'label' => 'NISN',
					'rules' => 'required|max_length[9999999999]'
				),
				array(
					'field' => 'nama_lengkap',
					'label' => 'Nama',
					'rules' => 'required'
				),
				array(
					'field' => 'tempat_lahir',
					'label' => 'Tempat Lahir',
					'rules' => 'required'
				),
				array(
					'field' => 'tgl_lahir',
					'label' => 'Tanggal Lahir',
					'rules' => 'required'
				),
				array(
					'field' => 'alamat',
					'label' => 'Alamat',
					'rules' => 'required'
				),
				array(
					'field' => 'asal_sekolah',
					'label' => 'Asal Sekolah',
					'rules' => 'required'
				),
				array(
					'field' => 'tahun_ajaran',
					'label' => 'Tahun Ajaran',
					'rules' => 'required'
				)
			);
			$this->form_validation->set_rules($rules);
				
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('notif', 'Periksa data!');
				redirect('admin/edit-siswa/'.$nisn);
			}
			else
			{
				$update_array = array(
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tgl_lahir'    => $this->input->post('tgl_lahir'),
					'alamat'       => $this->input->post('alamat'),
					'asal_sekolah' => $this->input->post('asal_sekolah'),
					'tahun_ajaran' => $this->input->post('tahun_ajaran'),
					'updated_at'   => date('Y-m-d H:i:s'),
					'id_users'     => $this->session->userdata('id')
				);
				// print_r($nisn." ".$this->input->post('nisn'));
				
				$update_db = $this->m_admin->set_data_update('tbl_siswa', $update_array, array('nisn'=>$nisn));
				
				if ($update_db)
				{
					$this->session->set_flashdata('notif', 'Data berhasil diubah!');
					redirect('admin/data-siswa');
				}
				else
				{
					echo 'gagal';
				}
			}
		}
		else
		{
			$this->data['title'] = 'Template Admin';
			$query = $this->m_admin->get_data_where('tbl_siswa', ['nisn'=>$nisn]);
			if ($query->num_rows() == 0)
			{
				$this->session->set_flashdata('notif', 'Data tidak ditemukan!');
				redirect('admin/data-siswa');
			}
			else
			{
				$this->data['data'] = $query->result();
				$this->load->view('admin/v_dataSiswaEdit', $this->data);
			}
		}
	}
	
	public function delete_siswa($nisn)
	{
		$query = $this->m_admin->set_delete('tbl_siswa', array('nisn'=>$nisn));
		
		if ($query)
		{
			$this->session->set_flashdata('notif', 'Data berhasil dihapus!');
			redirect('admin/data-siswa');
		}
	}
	
	// CRUD USERS
	public function data_users()
	{
		$query = $this->m_admin->get_data('tbl_users');

		if ($query->num_rows() > 0)
		{
			$this->data['row'] = $query->result();
		}
		else
		{
			// Data kosong
			$this->data['row'] = FALSE;
		}
		$this->data['box_title'] = "<i class='fa fa-users'></i> Data Users";

		$this->load->view('admin/v_dataUsers', $this->data);
	}

	public function tambah_users()
	{	
		$this->load->view('admin/v_dataUsersTambah', $this->data);
	}

	public function validate_users()
	{
		$id_users = $this->input->post('id_users');
		
		$rules = array(
			array(
				'field' => 'id_users',
				'label' => 'ID Users',
				'rules' => 'required'
			),
			array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required'
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|min_length[8]'
			),
			array(
				'field' => 'nama_lengkap',
				'label' => 'Nama',
				'rules' => 'required'
			),
			
			array(
				'field' => 'level',
				'label' => 'Level',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($rules);
			
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', 'Periksa data!');
			$this->tambah_users();
		}
		else
		{
			$input_array = array(
				'id_users'     => $this->input->post('kd_users'),
				'username'     => $this->input->post('username'),
				'nama_lengkap' => ucwords($this->input->post('nama_lengkap')),
				'password'     => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'level'        => $this->input->post('level'),
				'created_at'   => date('Y-m-d H:i:s')
			);
			$insert_db = $this->m_admin->set_data('tbl_users', $input_array);
			
			if ($insert_db)
			{
				$this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
				redirect('admin/tambah-users/');
			}
			else
			{
				$this->session->set_flashdata('notif', 'Data gagal ditambahkan!');
				redirect('admin/data-users');
			}
		}
		
	}
	
	public function edit_users($id_users)
	{
		if ($this->input->post('btn_edit'))
		{
			$rules = array(
				array(
					'field' => 'id_users',
					'label' => 'ID Users',
					'rules' => 'required'
				),
				array(
					'field' => 'username',
					'label' => 'Username',
					'rules' => 'required'
				),
				array(
					'field' => 'nama_lengkap',
					'label' => 'Nama',
					'rules' => 'required'
				),
				array(
					'field' => 'level',
					'label' => 'Level',
					'rules' => 'required'
				)
			);
			$this->form_validation->set_rules($rules);
				
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('notif', 'Periksa data!');
				redirect('admin/edit-users/'.$id_users);
			}
			else
			{
				$level  = strtoupper($this->input->post('level'));
				$res    = $this->m_admin->get_last_id_users($level)->result();
				$old_id = $this->input->post('id_users');
				$id     = $res[0]->id_users;
/*				
				if ($id == "" || $id == NULL)
				{
					if ($level == 'ADMIN')
					{
						$id_users = 'ADM01';
					}
					else
					{
						$id_users = 'GUR01';
					}
				}
				else
				{
					$id_start = substr($old_id, 0, 3);
					$id_akhir = substr($old_id, 3, 2);
					$id_akhir += 1;
					
					if ($id_akhir < 10)
					{
						$id_users = $id_start . "0" . $id_akhir;
					}
					else
					{
						$id_users = $id_start.$id_akhir;
					}
				}
*/				
				$update_array = array(
					// 'id_users'     => $id_users,
					'username'     => $this->input->post('username'),
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					// 'level'        => $level,
					'updated_at'   => date("Y-m-d H:i:s")
				);
				// print_r($update_array);
				$update_db = $this->m_admin->set_data_update('tbl_users', $update_array, array('id_users'=>$old_id));
				// var_dump($update_db);
				if ($update_db)
				{
					$this->session->set_flashdata('notif', 'Data berhasil diubah!');
					redirect('admin/data-users');
				}
				else
				{
					echo 'gagal';
				}
			}
		}
		else
		{
			$this->data['title'] = 'Template Admin';
			$query = $this->m_admin->get_data_where('tbl_users', ['id_users'=>$id_users]);
			if ($query->num_rows() == 0)
			{
				$this->session->set_flashdata('notif', 'Data tidak ditemukan!');
				redirect('admin/data-users');
			}
			else
			{
				$this->data['data'] = $query->result();
				$this->load->view('admin/v_dataUsersEdit', $this->data);
			}
		}
	}
	
	public function delete_users($id_users)
	{
		$query = $this->m_admin->set_delete('tbl_users', array('id_users'=>$id_users));
		
		if ($query)
		{
			$this->session->set_flashdata('notif', 'Data berhasil dihapus!');
			redirect('admin/data-users');
		}
	}
	
	public function get_id_users($level)
	{
		$res = $this->m_admin->get_last_id_users($level)->result();
			
		// var_dump($res[0]->id_users);
		
		if ($res[0]->id_users == "" || $res[0]->id_users == NULL)
		{
			if ($level == 'ADMIN')
			{
				$id_users = 'ADM01';
			}
			else
			{
				$id_users = 'GUR01';
			}
		}
		else
		{
			$id = $res[0]->id_users;
			$id_start = substr($id, 0, 3);
			$id_akhir = substr($id, 3, 2);
			$id_akhir += 1;
			
			if ($id_akhir < 10)
			{
				$id_users = $id_start . "0" . $id_akhir;
			}
			else
			{
				$id_users = $id_start.$id_akhir;
			}
		}
		echo $id_users;
		
	}
	
	
}

/* End of file Admin.php */
