<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('m_guru');
	}
	
	public function isLoggedIn()
	{
		$isLoggedIn   = $this->session->userdata('isLoggedIn');
		$isLevel = $this->session->userdata('level');

		if (!isset($isLoggedIn) || $isLoggedIn == FALSE)
		{
			$this->session->set_flashdata('notif', 'Anda belum melakukan login!');
			redirect('welcome');
		}

		if ($isLevel !== 'guru')
		{
			$this->session->set_userdata('isLoggedIn', FALSE);
			$this->session->set_flashdata('notif', 'Anda belum melakukan login sebagai guru!');
			redirect('welcome');
		}
	}
	
	public function index()
	{
		$data['title']          = 'Template Guru';
		$data['content_header'] = '<h1>Peminatan Peserta Didik Dengan Metode <i>Weighted Product</i></h1>';
		$data['title_header']   = 'Peminatan Peserta Didik Dengan Metode <i>Weighted Product</i>';
		$data['box_body']       = ucwords('sistem pendukung keputusan peminatan peserta didik dengan menggunakan metode <i>Weighted Product</i>');

		$this->load->view('guru/v_index', $data);
	}
	
	public function cari()
	{
		$data['title'] = 'Template Guru';
		$data['data']  = $this->m_guru->get_data('tbl_siswa');
		$this->load->view('guru/v_dataCari', $data);
	}

	public function input_nilai()
	{
		$nisn  = $this->input->post('nisn');
		if(!$nisn) redirect('guru/cari');
		$query = $this->m_guru->get_search(['tbl_siswa.nisn'=>$nisn]);
		if ($query->num_rows() == 0)
		{
			$this->session->set_flashdata('notif', 'NISN tidak ditemukan !');
			redirect('guru/cari');
		}
		else
		{
			$data['title'] = 'Template Guru';
			$data['data']  = $query;
			$this->load->view('guru/nilai/v_inputNilaiSiswa', $data);
		}
	}

	public function data_nilai()
	{
		redirect('nilai/data-nilai');
	}

	public function edit_nilai($id)
	{
		if (!$this->input->post('btn_update'))
		{
			$this->session->set_flashdata('notif', 'silahkan masukkan NISN !');
			redirect('guru/cari');
		}

		// set rules
		for ($i=1;$i<=6;$i++)
		{
			$this->form_validation->set_rules('ipasm'.$i, 'Nilai IPA Semester '.$i, 'trim|required|decimal', ['decimal'=>'%s ubah koma dengan tanda titik', 'required'=>'%s wajib diisi']);
			$this->form_validation->set_rules('ipssm'.$i, 'Nilai IPS Semester '.$i, 'trim|required|decimal', ['decimal'=>'%s ubah koma dengan tanda titik', 'required'=>'%s wajib diisi']);
			$this->form_validation->set_rules('mtksm'.$i, 'Nilai Matematika Semester '.$i, 'trim|required|decimal', ['decimal'=>'%s ubah koma dengan tanda titik', 'required'=>'%s wajib diisi']);
		}

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', 'Harap periksa kembali !');
			$this->input_nilai();
		}
		else
		{
			$nisn     = $this->input->post('nisn');
			$rata_ipa = 0;
			$rata_ips = 0;
			$rata_mtk = 0;

			for ($i=1;$i<=6;$i++)
			{
				$data['ipasm'.$i] = $this->input->post('ipasm'.$i);
				$data['ipssm'.$i] = $this->input->post('ipssm'.$i);
				$data['mtksm'.$i] = $this->input->post('mtksm'.$i);

				$rata_ipa += $data['ipasm'.$i];
				$rata_ips += $data['ipssm'.$i];
				$rata_mtk += $data['mtksm'.$i];
				
				$nilai['count'] = $i;
			}

			// mencari rata ipa, ips, dan mtk
			$data['rata_ipa']   = (($rata_ipa/$nilai['count']) + ($rata_mtk/$nilai['count']))/2;
			$data['rata_ips']   = (($rata_ips/$nilai['count']) + ($rata_mtk/$nilai['count']))/2;
			$data['un_ipa']     = $this->input->post('un_ipa');
			$data['us_ips']     = $this->input->post('us_ips');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			// Simpan ke tabel nilai
			$upd = $this->m_guru->set_data_update('tbl_nilai', $data, ['nisn'=>$nisn]);

			// Prepare simpan ke tabel nilai alternatif
			$alt['nisn']    = $nisn;
			$alt['k4_mipa'] = $data['rata_ipa'];
			$alt['k5_mipa'] = $data['un_ipa'];
			$alt['k4_iis']  = $data['rata_ips'];
			$alt['k5_iis']  = $data['us_ips'];
			$alt['created_at'] = date('Y-m-d H:i:s');

			// cek nisn di tabel nilai alternatif
			$get = $this->m_guru->get_data_where('tbl_nilai_alternatif', ['nisn'=>$nisn]);

			if ($get->num_rows() > 0)
			{
				// jika data sudah ada, update nilai kriteria 1-3
				$upd_k = $this->m_guru->set_data_update('tbl_nilai_alternatif', $alt, ['nisn'=>$nisn]);
				$this->session->set_flashdata('success', 'Data berhasil ditambahkan !');
				redirect('nilai/data-nilai');
			}
			else
			{
				$ins_nilai_alternatif = $this->m_guru->set_data('tbl_nilai_alternatif', $alt);
				$this->session->set_flashdata('success', 'Data berhasil ditambahkan !');
				redirect('nilai/data-nilai');
			}
		}
	}

	public function detail($id)
	{
		$data['title']     = "Template Guru";
		$data['box_title'] = "Detail Informasi";
		$data['data']      = $this->m_guru->detailSiswa(['tbl_siswa.nisn'=>$id]);

		$this->load->view('guru/nilai/v_detailSiswa', $data);
	}

	public function input_bobot()
	{
		redirect('bobot/set-bobot');
	}

	public function perhitungan()
	{
		$data['title']     = 'Template Guru';
		$data['box_title'] = 'Analisa Perhitungan';

		$this->load->model('m_nilai');
		$db_alternatif = $this->m_nilai->get_nilaiAlt();
		$db_bobot      = $this->db->get('tbl_bobot_kriteria');

		if ($this->input->post('btn_hitung'))
		{
			$data['db_bobot']      = $db_bobot;
			$data['db_alternatif'] = $db_alternatif;
			$data['db_analisa']    = TRUE;

			// Proses hitung vektor S
			$res_bobot    = $data['db_bobot']->result();
			$res_kriteria = $data['db_alternatif']->result();

			$n = 0;
			$S_mipa = [];
			$S_iis  = [];
			$V_mipa = [];
			$V_iis  = [];

			for ($i=0; $i < count($res_kriteria); $i++)
			{
				$S_mipa['S1'] = pow($res_kriteria[$i]->k1_mipa, $res_bobot[0]->bobot_perbaikan);
				$S_mipa['S2'] = pow($res_kriteria[$i]->k2_mipa, $res_bobot[1]->bobot_perbaikan);
				$S_mipa['S3'] = pow($res_kriteria[$i]->k3_mipa, $res_bobot[2]->bobot_perbaikan);
				$S_mipa['S4'] = pow($res_kriteria[$i]->k4_mipa, $res_bobot[3]->bobot_perbaikan);
				$S_mipa['S5'] = pow($res_kriteria[$i]->k5_mipa, $res_bobot[4]->bobot_perbaikan);
				
				$S_iis['S1'] = pow($res_kriteria[$i]->k1_iis, $res_bobot[0]->bobot_perbaikan);
				$S_iis['S2'] = pow($res_kriteria[$i]->k2_iis, $res_bobot[1]->bobot_perbaikan);
				$S_iis['S3'] = pow($res_kriteria[$i]->k3_iis, $res_bobot[2]->bobot_perbaikan);
				$S_iis['S4'] = pow($res_kriteria[$i]->k4_iis, $res_bobot[3]->bobot_perbaikan);
				$S_iis['S5'] = pow($res_kriteria[$i]->k5_iis, $res_bobot[4]->bobot_perbaikan);

				$id_alternatif = $res_kriteria[$i]->id_alternatif;
				$S_total       = array_sum($S_mipa) + array_sum($S_iis);
				$V_mipa        = array_sum($S_mipa) / $S_total;
				$V_iis         = array_sum($S_iis) / $S_total;
				$jurusan       = ($V_mipa > $V_iis) ? "MIPA" : "IIS";

				// prepare simpan ke database
				$save_db['id_alternatif']     = $id_alternatif;
				$save_db['s_mipa']            = array_sum($S_mipa);
				$save_db['s_iis']             = array_sum($S_iis);
				$save_db['v_mipa']            = $V_mipa;
				$save_db['v_iis']             = $V_iis;
				$save_db['jurusan_prioritas'] = $jurusan;
				$save_db['created_at']        = date("Y-m-d H:i:s");

				// cek duplikasi data
				$cek = $this->db->get_where('tbl_hasil', ['id_alternatif'=>$save_db['id_alternatif']]);

				if ($cek->num_rows() > 0)
				{
					// $this->session->set_flashdata('notif', 'Sebagian data sudah pernah tersimpan!');
					// empty tabel hasil
					$this->db->truncate('tbl_hasil');
					$this->m_nilai->set_hasilAnalisa($save_db);
					$this->session->set_flashdata('success', 'Perhitungan berhasil disimpan!');
					$data['db_bobot'] = $db_bobot;
				}
				else
				{
					// simpan ke database
					$save = $this->m_nilai->set_hasilAnalisa($save_db);
					if ($save)
					{
						$this->session->set_flashdata('success', 'Perhitungan berhasil disimpan!');
						$data['db_bobot'] = $db_bobot;
					}
				}
			}
			$data['db_hasil'] = $this->m_nilai->get_hasilAnalisa();

			$this->load->view('guru/nilai/v_analisa', $data);
		}
		else
		{
			$data['db_bobot']      = $db_bobot;
			$data['db_alternatif'] = $db_alternatif;

			$this->load->view('guru/nilai/v_analisa', $data);
		}
	}

	public function ubah_password()
	{
		$data['title'] = 'Template Guru';
		$this->load->view('v_ubah_password', $data);
	}

	public function change_password()
	{
		if ($this->input->post('btn_submit'))
		{
			$this->form_validation->set_rules('new_password', 'Password Baru', 'required|min_length[8]');
			$this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[new_password]');

			if ($this->form_validation->run() == FALSE)
			{
				$this->ubah_password();
			}
			else
			{
				$new_password  = $this->input->post('new_password');
				$pass['password'] = password_hash($new_password, PASSWORD_DEFAULT);

				$upd_pass = $this->m_guru->set_data_update('tbl_users', $pass, ['id_users'=>$this->session->userdata('id')]);

				$this->session->set_flashdata('notif', 'Password berhasil diubah. Silahkan login kembali !');
				redirect('welcome');
				
			}
		}
		else
		{
			$this->session->set_flashdata('notif', 'Error !');
			redirect('guru/ubah_password/'.$this->session->userdata('id'));
		}
	}

	public function laporan()
	{
		redirect('laporan/detail_laporan');
	}
	
}

/* End of file Guru.php */
