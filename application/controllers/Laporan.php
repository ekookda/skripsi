<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('pdf');
		$this->load->model(['m_laporan', 'm_nilai']);
	}

	public function detail_laporan()
	{
		$data['hasil'] = $this->m_nilai->get_hasilAnalisa();
		$this->load->view('laporan/v_contoh', $data);
	}
	
}

/* End of file Laporan.php */
