<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form Login';
		$this->load->view('login', $data);
	}

	public function validasi_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level    = $this->input->post('level');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
		$this->form_validation->set_rules('level', 'Level', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', 'Silahkan login terlebih dahulu');
			redirect('welcome');
		}
		else
		{
			// load model login
			$this->load->model('m_login');

			// decision level
			if ($level == 'siswa')
			{
				$query = $this->m_login->get_data('tbl_siswa', ['nisn'=>$username]);
			}
			elseif ($level == 'admin' || $level == 'guru')
			{
				$query = $this->m_login->get_data('tbl_users', ['username'=>$username, 'level'=>$level]);
			}
			else
			{
				$query = FALSE;
				$this->session->set_flashdata('notif', 'Silahkan login terlebih dahulu!');
				redirect('welcome');
			}

			$row     = $query->result();
			$pass_db = $row[0]->password;
			
			if (password_verify($password, $pass_db))
			{
				$sess_data = [
					'nama_lengkap' => $row[0]->nama_lengkap,
					'username'     => $row[0]->username,
					'id'           => ($level == 'siswa') ? $row[0]->nisn : $row[0]->id_users,
					'isLoggedIn'   => TRUE
				];
				$this->session->set_userdata($sess_data);
				
				// updated_at login
				$timestamp = date('Y-m-d H:i:s');

				if ($level == 'admin')
				{
					$this->m_login->updated_at('tbl_users', ['id_users'=>$this->session->userdata('id')], ['updated_at'=>$timestamp]);
					$this->session->set_userdata('level', 'admin');
					redirect('admin');
				}
				elseif ($level == 'guru')
				{
					$this->m_login->updated_at('tbl_users', ['id_users'=>$this->session->userdata('id')], ['updated_at'=>$timestamp]);
					$this->session->set_userdata('level', 'guru');
					redirect('guru');
				}
				else
				{
					$this->m_login->updated_at('tbl_siswa', ['nisn'=>$this->session->userdata('id')], ['updated_at'=>$timestamp]);
					$this->session->set_userdata('level', 'siswa');
					redirect('siswa');
				}
			}
			else
			{
				$this->session->set_flashdata('notif', 'Username atau Password salah!');
				redirect('Welcome');
			}
		}
	}

	public function logout()
	{
		// mengakhiri semua session
		$this->session->sess_destroy();
		redirect('welcome');
	}

}

/* End of file Welcome.php */
