<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class Nilai extends CI_Controller 
{
	private $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('m_nilai');
		$this->data['title'] = 'Template Guru';
	}

	public function isLoggedIn()
	{
		$isLoggedIn   = $this->session->userdata('isLoggedIn');
		$isLevel = $this->session->userdata('level');

		if (!isset($isLoggedIn) || $isLoggedIn == FALSE)
		{
			$this->session->set_flashdata('notif', 'Anda belum melakukan login!');
			redirect('welcome');
		}

		if ($isLevel !== 'guru')
		{
			$this->session->set_userdata('isLoggedIn', FALSE);
			$this->session->set_flashdata('notif', 'Anda belum melakukan login sebagai guru!');
			redirect('welcome');
		}
	}

	public function data_nilai()
	{
		$query = $this->m_nilai->get_dataNilai();
		$this->data = [
			'title' => $this->data['title'],
			'box_title' => '<i class="fa fa-info-circle"></i> Data Nilai Peserta Didik',
			'data' => $query->result(),
		];
		
		$this->load->view('guru/nilai/v_dataNilaiSiswa', $this->data);
	}

}

/* End of file Nilai.php */
