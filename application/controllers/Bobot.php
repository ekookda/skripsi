<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bobot extends CI_Controller
{
	public $data = [];
	
	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('m_guru');
		$this->data['title'] = 'Template Guru';
	}
	
	public function isLoggedIn()
	{
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		$isLevel    = $this->session->userdata('level');

		if (!isset($isLoggedIn) || $isLoggedIn == FALSE)
		{
			$this->session->set_flashdata('notif', 'Anda belum melakukan login!');
			redirect('welcome');
		}

		if ($isLevel !== 'guru')
		{
			$this->session->set_userdata('isLoggedIn', FALSE);
			$this->session->set_flashdata('notif', 'Anda belum melakukan login sebagai guru!');
			redirect('welcome');
		}
	}
	
	public function index()
	{
		$this->data['title'] = $this->data['title'];
		$this->data['content_header'] = '<h1>Peminatan Peserta Didik Dengan Metode <i>Weighted Product</i></h1>]';
		$this->data['box_body'] = ucwords('sistem pendukung keputusan peminatan peserta didik dengan menggunakan metode <i>Weighted Product</i>');
		$this->data['content_header'] = '';
		$this->load->view('guru/v_index', $this->data);
	}
	
	public function set_bobot()
	{
		$query = $this->db->get('tbl_bobot_kriteria');
		$this->data['data'] = $query->result();
		$this->load->view('guru/bobot/v_dataInputBobot', $this->data);
	}
	
	public function save()
	{
		if ($this->input->post('btn_nilai'))
		{
			$this->form_validation->set_rules('kriteria1', 'Kriteria 1', 'required');
			$this->form_validation->set_rules('kriteria2', 'Kriteria 2', 'required');
			$this->form_validation->set_rules('kriteria3', 'Kriteria 3', 'required');
			$this->form_validation->set_rules('kriteria4', 'Kriteria 4', 'required');
			$this->form_validation->set_rules('kriteria5', 'Kriteria 5', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->set_bobot();
			}
			else
			{
				// Ambil nilai bobot/nilai awal
				$nilai_awal['K01'] = $this->input->post('kriteria1');
				$nilai_awal['K02'] = $this->input->post('kriteria2');
				$nilai_awal['K03'] = $this->input->post('kriteria3');
				$nilai_awal['K04'] = $this->input->post('kriteria4');
				$nilai_awal['K05'] = $this->input->post('kriteria5');

				// Hitung perbaikan bobot
				$k = array();
				$n = 1;
				$jumlah_bobot = array_sum($nilai_awal);
				while ($n <= count($nilai_awal))
				{
					$data['bobot_awal'] = $nilai_awal['K0'.$n];
					$data['bobot_perbaikan'] = $nilai_awal['K0'.$n]/$jumlah_bobot;

					// Simpan ke database berdasarkan kriteria
					$query = $this->m_guru->set_data_update('tbl_bobot_kriteria', $data, ['id_kriteria'=>'K0'.$n]);
					$n++;
				}

				$this->session->set_flashdata('success', 'Nilai bobot kriteria berhasil disimpan');
				redirect('bobot/set-bobot');
			}
		}
		else
		{
			$this->set_bobot();
		}
	}
	
}

/* End of file Bobot.php */
