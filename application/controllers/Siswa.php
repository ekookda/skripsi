<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('m_siswa');
	}

	public function isLoggedIn()
	{
		$isLoggedIn   = $this->session->userdata('isLoggedIn');
		$isLevel = $this->session->userdata('level');

		if (!isset($isLoggedIn) || $isLoggedIn == FALSE)
		{
			$this->session->set_flashdata('notif', 'Anda belum melakukan login!');
			redirect('welcome');
		}

		if ($isLevel !== 'siswa')
		{
			$this->session->set_userdata('isLoggedIn', FALSE);
			$this->session->set_flashdata('notif', 'Anda belum melakukan login sebagai siswa!');
			redirect('welcome');
		}
	}

	public function ubah_password()
	{
		$data['title'] = "Template Siswa";
		$this->load->view('v_ubah_password', $data);
	}

	public function change_password()
	{
		if ($this->input->post('btn_submit'))
		{
			$this->form_validation->set_rules('new_password', 'Password Baru', 'required|min_length[8]');
			$this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[new_password]');

			if ($this->form_validation->run() == FALSE)
			{
				$this->ubah_password();
			}
			else
			{
				$new_password  = $this->input->post('new_password');
				$pass['password'] = password_hash($new_password, PASSWORD_DEFAULT);

				$upd_pass = $this->m_siswa->set_data_update('tbl_siswa', $pass, ['nisn'=>$this->session->userdata('id')]);

				$this->session->set_flashdata('notif', 'Password berhasil diubah. Silahkan login kembali !');
				redirect('welcome');
				
			}
		}
		else
		{
			$this->session->set_flashdata('notif', 'Error !');
			redirect('siswa/ubah_password/'.$this->session->userdata('id'));
		}
	}

	public function index()
	{
		$data['title'] = 'Template Siswa';
		$data['box_title'] = '';
		$data['content_header'] = '<h1>Peminatan Peserta Didik Dengan Metode <i>Weighted Product</i></h1>';
		$data['title_header'] = 'Peminatan Peserta Didik Dengan Metode <i>Weighted Product</i>';
		$data['box_body'] = ucwords('sistem pendukung keputusan peminatan peserta didik dengan menggunakan metode <i>Weighted Product</i>');

		$this->load->view('siswa/v_index', $data);
	}

	public function angket()
	{
		$nisn = $this->session->userdata('id');

		// cek sudah pernah mengisi angket atau belum ?

		$data['title'] = 'Template Siswa';
		$data['box_title'] = 'Pengisian Angket';
		
		$this->load->view('siswa/v_inputAngket', $data);
	}

	public function isi_angket()
	{
		if ($this->input->post('btn_angket'))
		{
			$nisn = $this->session->userdata('id');
			
			if (empty($this->input->post('mapel_mipa')) && empty($this->input->post('mapel_iis')))
			{
				$this->session->set_flashdata('notif', 'Pilih salah satu pelajaran yang diminati');
				redirect('siswa/angket','refresh');
			}
			
			// cek sudah pernah mengisi atau belum
			// $db = $this->m_siswa->get_where('tbl_nilai_alternatif', ['nisn'=>$nisn]);
			// if ($db->num_rows() > 0)
			// {
			// 	$this->session->set_flashdata('notif', 'Anda sudah pernah mengisi angket !');
			// 	redirect('siswa/angket');
			// }

			$pesan_error['required'] = '<span class="text-danger">%s pilih salah satu</span>';

			// validasi
			$this->form_validation->set_rules('minat_jurusan', 'Minat Jurusan', 'required');
			$this->form_validation->set_rules('minat_orangtua', 'Pilihan Orang Tua', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('notif', 'Data wajib diisi');
				redirect('siswa/angket');
			}
			else 
			{
				$count = [];
				$angket = [];

				$count['mipa']  = count($this->input->post('mapel_mipa'));
				$count['iis']   = count($this->input->post('mapel_iis'));
				$count['merge'] = ($count['iis'] > 0 && $count['mipa'] > 0) ? array_merge($this->input->post('mapel_iis'), $this->input->post('mapel_mipa')) : "";

				if ($this->input->post('minat_jurusan') == 'mipa')
				{
					$angket['k1_mipa'] = 2;
					$angket['k1_iis']  = 1;
				}
				elseif ($this->input->post('minat_jurusan') == 'iis')
				{
					$angket['k1_iis']  = 2;
					$angket['k1_mipa'] = 1;
				}

				if ($this->input->post('minat_orangtua') == 'mipa')
				{
					$angket['k2_mipa'] = 2;
					$angket['k2_iis']  = 1;
				}
				elseif ($this->input->post('minat_orangtua') == 'iis')
				{
					$angket['k2_iis']  = 2;
					$angket['k2_mipa'] = 1;
				}

				if ($count['mipa'] >= $count['iis'])
				{
					$angket['k3_mipa'] = 2;
					$angket['k3_iis']  = 1;
				}
				else
				{
					$angket['k3_iis']  = 2;
					$angket['k3_mipa'] = 1;
				}

				$data_siswa['minat_jurusan'] = $this->input->post('minat_jurusan');
				$data_siswa['minat_ortu']    = $this->input->post('minat_orangtua');

				if ($count['iis'] > 0 && $count['mipa'] > 0)
				{
					$data_siswa['minat_mapel'] = implode(', ', $count['merge']);
				}
				elseif ($count['mipa'] > 0)
				{
					$data_siswa['minat_mapel'] = implode(', ', $this->input->post('mapel_mipa'));
				}
				elseif ($count['iis'] > 0)
				{
					$data_siswa['minat_mapel'] = implode(', ', $this->input->post('mapel_iis'));
				}
				else
				{
					$this->session->set_flashdata('notif', 'Pilih salah satu pelajaran yang diminati');
					redirect('siswa/angket');
				}

				// simpan ke tabel siswa
				$query = $this->m_siswa->set_angket($data_siswa, ['nisn'=>$nisn]);
				// Simpan ke tabel nilai alternatif
				$angket['updated_at'] = date('Y-m-d H:i:s');
				$this->db->where(['nisn'=>$nisn]);
				$this->db->update('tbl_nilai_alternatif', $angket);

				if ($query)
				{
					// Berhasil menyimpan ke database
					$this->session->set_flashdata('success', 'Terima kasih atas partisipasinya!');
					redirect('siswa');
				}
				else
				{
					// sudah pernah mengisi angket
					$this->session->set_flashdata('notif', 'Angket sudah pernah diisi !');
					redirect('siswa/angket');
				}
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function hasil()
	{
		$data['title'] = 'Template Siswa';
		$data['box_title'] = 'Detail Informasi';
		$data['data'] = $this->m_siswa->hasilAnalisa(['tbl_siswa.nisn'=>$this->session->userdata['id']]);

		$this->load->view('siswa/v_hasilAnalisa', $data);
	}


/* 
	public function validasi_login()
	{
		$nisn     = $this->input->post('nisn');
		$password = $this->input->post('password');

		$rules = array(
			array(
				'field' => 'nisn',
				'label' => 'nisn',
				'rules' => 'required'
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', 'Silahkan login terlebih dahulu');
			redirect('welcome');
		}
		else
		{
			$this->load->model('m_login');
			$query = $this->m_login->get_data($nisn);
			$row = $query->row();

			$pass_db = $row->password;
			$leveldb = $row->level;

			if (password_verify($password, $pass_db))
			{
				$sess_data = array(
					'isLoggedIn'   => true,
					'nama_lengkap' => $row->nama_lengkap,
					'nisn' => $row->nisn,
					'id_users'     => $row->id_users
				);
				$this->session->set_userdata($sess_data);

				// updated_at login
				$timestamp = date('Y-m-d H:i:s');
				$this->m_login->updated_at($row->id_users, array('updated_at'=>$timestamp));

				if ($leveldb == 'ADMIN')
				{
					$this->session->set_userdata('level', 'ADMIN');
					redirect('admin');
				}
				else
				{
					$this->session->set_userdata('level', 'GURU');
					redirect('guru');
				}
			}
			else
			{
				$this->session->set_flashdata('notif', 'nisn atau Password salah!');
				redirect('Welcome');
			}
		}
	}
 */

}

/*  End of file Siswa.php */
