<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']      = 'welcome';
$route['404_override']            = '';
$route['translate_uri_dashes']    = TRUE;

$route['admin/data-siswa']        = 'admin/data_siswa';
$route['admin/edit-siswa/(:any)'] = 'admin/edit_siswa/$1';
$route['admin/data-users']        = 'admin/data_users';
$route['admin/edit-users/(:any)'] = 'admin/edit_users/$1';

$route['guru/(:any)']             = 'guru/$1';
$route['guru/(:any)/(:any)']      = 'guru/$1/$2';
$route['guru/input-bobot']        = 'bobot/set_bobot';
$route['guru/data-nilai']         = 'nilai/data_nilai';
$route['laporan']                 = 'laporan/detail_laporan';

$route['siswa/hasil-analisa']     = 'siswa/hasil';
